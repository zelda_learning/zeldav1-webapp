// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase :
    {
      apiKey: "AIzaSyDYZfVupq-8AnpdOvwH6aWhAK5FfMiehTw",
      authDomain: "zeldav1-dev.firebaseapp.com",
      databaseURL: "https://zeldav1-dev.firebaseio.com",
      projectId: "zeldav1-dev",
      storageBucket: "zeldav1-dev.appspot.com",
      messagingSenderId: "317546250483"
    },
    bucket : "gs://zelda-dev-ml-course-bucket"
};
