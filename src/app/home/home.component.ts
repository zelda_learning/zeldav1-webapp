import { Component, OnInit } from '@angular/core';
import {UserProfile} from '../objects/userProfile';
import {ProfileService} from '../profile.service';
import {FirebaseIntegrationService} from '../firebase-integration.service';
import {filter, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {FlashMessagesService} from 'angular2-flash-messages';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  callsToAction: any [];
  optIn = false;

  constructor(public profile: ProfileService, private database: FirebaseIntegrationService, private fm: FlashMessagesService) { }

  ngOnInit() {

    //this.fm.show('We are in about component!', { cssClass: 'alert-success', timeout: 10000 });
    this.database.noMenu = false;
    this.database.loggedIn = true;
    this.database.getUserState().then(us => {
      us.pipe(
        map((userState: any) => {
          this.optIn = userState.optIn;
          return  userState.callsToAction;
        }),
        switchMap(calls => {
          this.callsToAction = [];
          return of(calls).pipe(
            map(call => {
              return call;
            })
          );
        }),
        filter((id: string) => {
          if (id && id.length !== 0) {
            return true;
          } else { return false; }
        }),
        switchMap((id: string) => {
          console.log(id, 'id');
          return this.database.getApplication(id);
        })
      ).subscribe(data => {
        if (data) {
          this.callsToAction.push(data);
          console.log(data, 'bursary called');
        }
      });
    });
  }

}
