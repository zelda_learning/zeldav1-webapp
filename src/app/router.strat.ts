import {ActivatedRouteSnapshot, DetachedRouteHandle, RouteReuseStrategy} from '@angular/router';


export class CustomReuseStrategy implements RouteReuseStrategy {

  private handlers: {[key: string]: DetachedRouteHandle} = {};
  private preservedComps = [
    'ProfileComponent',
    'HomeComponent',
    'ApplicationFormComponent',
    'ApplicationFormFragmentComponent'
  ];


  constructor() {

  }

  shouldDetach(route: ActivatedRouteSnapshot): boolean {
    return route.data.shouldReuse || false;
  }

  store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
    let store  = true;
    if (!route.data.shouldReuse) {
      store = false;
    }
    if (store) {
      this.handlers[route.url.join('/') || route.parent.url.join('/')] = handle;
    }
  }

  shouldAttach(route: ActivatedRouteSnapshot): boolean {
    const url = route.url.join('/') || route.parent.url.join('/');

    if (!route.data.shouldReuse) {
      return false;
    }
    return !!this.handlers[url];

  }

  retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
    return this.handlers[route.url.join('/') || route.parent.url.join('/')];
  }

  shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    return future.routeConfig === curr.routeConfig;
  }

}
