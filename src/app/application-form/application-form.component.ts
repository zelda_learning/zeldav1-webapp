import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseIntegrationService} from'../firebase-integration.service';
import { map, switchMap, flatMap} from "rxjs/operators";
import {Observable} from "rxjs/observable";
import {ProfileService} from "../profile.service";
import {ApplicationService} from '../application.service';

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.scss']
})
export class ApplicationFormComponent implements OnInit {
  route : string = 'intro';
  id : string;
  uid : string;
  formId : string;
  userFormId : string;
  application : any;
  sections : number ;
  navItems : number[] = [];
  letters : string;
  submitted : boolean;
  verified = false;

  constructor(private router: ActivatedRoute, private database: FirebaseIntegrationService,
              public profile : ProfileService, public appService : ApplicationService, private router2 : Router) { }

  ngOnInit() {
    this.profile.getUserVerified().then(verified => {
      this.verified = verified;
    });
    this.letters = "ABCDEFGH";
    this.router.params.subscribe(url=>{
      if(url.comp && url.comp != ""){
        this.route = url.comp;
      }
      else{
        this.route = '0';
      }
      this.id = url.id;
      console.log(this.route);
      if(!this.appService.intro){
        this.router2.navigate(["/applications/"+this.id+"/intro"])
      }
      else{
        this.sections = 0;
        this.navItems = [];
        if (this.profile.userState.progress && this.profile.userState.progress !== 0) {
          console.log(url.id);
          this.database.getApplication(url.id).subscribe(app=>{
            console.log(app, "Application");
            this.sections = app['sections'];
            for(var i =0; i<this.sections; i++){
              this.navItems.push(i);
              //console.log(this.navItems);
            }
          });
          this.appService.init(this.id);
        }
        else{
          console.log(this.profile.userState.progress, 'no menu ?')
          this.database.noMenu = true;
          this.sections = 2;
          this.navItems = [0];
        }
      }
    });
  }

  getClass(nameIn: string){
    var classRet = "nav-link ";
    if(nameIn == this.route){
      classRet += 'active';
    }
    return classRet;
  }

}
