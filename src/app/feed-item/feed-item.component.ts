import { Component, OnInit, Input } from '@angular/core';
import { FirebaseIntegrationService} from '../firebase-integration.service';


@Component({
  selector: 'app-feed-item',
  templateUrl: './feed-item.component.html',
  styleUrls: ['./feed-item.component.scss']
})
export class FeedItemComponent implements OnInit {
  @Input() item : any;
  pic : String;
  cont : String;

  constructor(private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.pic ="";
    this.cont = this.item.subtitle.replace(/<\/?[^>]+(>|$)/g, "");
    this.database.getDownloadUrl("/Resources/" + this.item.id + ".jpg").subscribe(url =>{
      this.pic = url;
    });
  }

}
