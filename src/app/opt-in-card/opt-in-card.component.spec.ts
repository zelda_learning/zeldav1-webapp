import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OptInCardComponent } from './opt-in-card.component';

describe('OptInCardComponent', () => {
  let component: OptInCardComponent;
  let fixture: ComponentFixture<OptInCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptInCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OptInCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
