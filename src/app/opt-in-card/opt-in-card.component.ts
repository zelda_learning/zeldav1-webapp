import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../profile.service';
import {FirebaseIntegrationService} from '../firebase-integration.service';

@Component({
  selector: 'app-opt-in-card',
  templateUrl: './opt-in-card.component.html',
  styleUrls: ['./opt-in-card.component.scss']
})
export class OptInCardComponent implements OnInit {

  constructor(public profile: ProfileService, private database: FirebaseIntegrationService) { }

  ngOnInit() {
  }

  optIn() {
    this.database.createCBAForm().then(data=>{
      this.database.addCbaToState(data.id).then(
        data2 => {
          this.sendOptInCard();
          // console.log(data2);
        }
      );
    });
  }

  sendOptInCard = () => {
    (<any>window).ga('send', 'event', {
      eventCategory: 'OptIn',
      eventLabel: '',
      eventAction: 'OptInCard',
      eventValue: 1
    });
  }
}
