import { Component, OnInit } from '@angular/core';
import { University } from '../objects/university';
import { FirebaseIntegrationService } from '../firebase-integration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-university',
  templateUrl: './university.component.html',
  styleUrls: ['./university.component.scss']
})
export class UniversityComponent implements OnInit {
  universities : University[];
  filter : String;

  constructor(private database: FirebaseIntegrationService, private router: Router) { }

  ngOnInit() {
    this.filter = "All"
    this.database.getUniversities()
        .subscribe(data=>{
        this.universities = data;
      });
  }
}
