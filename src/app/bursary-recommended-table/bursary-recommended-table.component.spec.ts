import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BursaryRecommendedTableComponent } from './bursary-recommended-table.component';

describe('BursaryRecommendedTableComponent', () => {
  let component: BursaryRecommendedTableComponent;
  let fixture: ComponentFixture<BursaryRecommendedTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BursaryRecommendedTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BursaryRecommendedTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
