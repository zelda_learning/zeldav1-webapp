import { Component, OnInit} from '@angular/core';
import {Router} from '@angular/router'
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { ProfileService } from '../profile.service';
import { flatMap, filter} from 'rxjs/operators';
import {combineLatest, of} from "rxjs";
@Component({
  selector: 'app-bursary-recommended-table',
  templateUrl: './bursary-recommended-table.component.html',
  styleUrls: ['./bursary-recommended-table.component.scss']
})
export class BursaryRecommendedTableComponent implements OnInit {
  bursaries: Array<any>;
  recBursaries: Array<any> = [];
  constructor(private database: FirebaseIntegrationService, private profile: ProfileService,
    private router: Router) { }

  ngOnInit() {
    this.database.getApplications().pipe(
      flatMap(apps=>{
        return apps;
      }),
      filter(app=>{
        return this.contains(app);
      }),
      flatMap(app=>{
        return combineLatest(
          of(app),
          this.database.getDownloadUrl("Bursaries/"+app.id+".jpg")
        )
      })
    ).subscribe(data=>{
      data[0].image = data[1];
      data[0].subtitle = data[0].subtitle.replace(/<\/?[^>]+(>|$)/g, "");
      this.recBursaries.push(data[0]);
    });
  }

  contains(app : any){
    var found : boolean = false;

    for(var i=0; i<app.applicableFields.length && !found; i++){
      if(this.profile.userState.fieldPreference[app.applicableFields[i]]){
        found = true;
      }
    }
    return found
  }

  clickFav(fav){
    this.router.navigate(["/applications/"+fav.id])
  }

}
