import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationFormFragmentComponent } from './application-form-fragment.component';

describe('ApplicationFormFragmentComponent', () => {
  let component: ApplicationFormFragmentComponent;
  let fixture: ComponentFixture<ApplicationFormFragmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationFormFragmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationFormFragmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
