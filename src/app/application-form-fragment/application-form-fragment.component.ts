import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { Router} from '@angular/router';
import { ProfileService } from '../profile.service';
import { map, switchMap} from 'rxjs/operators';
import {combineLatest} from 'rxjs';
import {ApplicationService} from '../application.service';

@Component({
  selector: 'app-application-form-fragment',
  templateUrl: './application-form-fragment.component.html',
  styleUrls: ['./application-form-fragment.component.scss']
})
export class ApplicationFormFragmentComponent implements OnInit, OnDestroy {
  @Input() section: number;
  submitted = false;
  @Input() numSections: number;
  id: string;
  formValuesNames: string[];
  formAnswers: any = {};
  formValues: any [];
  formOptions: any;
  letters = 'ABCDEFGH';
  address = {line1: '', line2: '', zip: '', city: ''};
  verified = true;
  unAuth = false;

  line1 = '';
  line2 = '';
  city = '';
  province = '';

  constructor(private profile: ProfileService,
              private database: FirebaseIntegrationService,
              private router: ActivatedRoute,
              private router2: Router,
              private appService: ApplicationService) { }

  ngOnInit() {
    this.profile.getUserVerified().then(verified => {
      this.verified = verified;
      this.unAuth = this.appService.unAuth;
      console.log(this.submitted, this.unAuth, this.verified);
    });
    if (this.profile.userState.progress && this.profile.userState.progress !== 0) {
      this.appService.getAppDocID().then(id => {
        console.log(id + ' ID is ready');
        this.router.params.pipe(
          map(url => {
            this.id = url.id;
            return url.id;
          }),
          switchMap(id => {
            console.log(this.appService.userFormId + ' ' + this.letters[this.section]);
            return combineLatest(
              this.database.getApplication(this.id),
              this.database.getApplicationForm(this.id, this.letters[this.section]),
              this.database.getUserApplicationForm(this.appService.userFormId)
            )
            .pipe(
              map(([app, form, uaf]) => {
                console.log(app);
                console.log(form);
                console.log(uaf);
                return {app, form, uaf};
              })
            );
          }),
          switchMap(({app, form, uaf}) => {
            console.log(app);
            console.log(form);
            console.log(uaf);
            if (app) {
              this.numSections = app.sections;
            }

            if (form) {
              this.formValues = form.formValues;
              this.formOptions = form;
              this.formValuesNames = Object.keys(form.formValues);
              for (const i of this.formValuesNames) {
                this.formAnswers[i] = '';
              }
            }

            if (uaf) {
              console.log(uaf);
              this.submitted = uaf.submitted;
            }
            return this.database.getUserFormFragment(this.appService.userFormId, this.letters[this.section]);
          })
        ).subscribe((uaf) => {
          console.log(uaf);
          if (uaf && uaf.formValues) {
            this.appService.form[this.letters[this.section]] = uaf;
            for (const i of Object.keys(uaf.formValues)) {
              this.formAnswers[i] = uaf.formValues[i];
              if (i.indexOf('address') > -1) {
                const pieces = this.formAnswers[i].split('$');
                console.log(pieces);

                this.line1 = pieces[0];
                if (pieces[1]) {
                  console.log(pieces[1]);
                  this.line2 = pieces[1];
                }
                if (pieces[2]) {
                  this.city = pieces[2];
                }
                if (pieces[3]) {
                  this.province = pieces[3];
                }
              }
            }
          } else {
            const newFormValues = {formValues: {}};
            for (const i of this.formValuesNames) {
                newFormValues.formValues[i] = this.formAnswers[i];
            }
            this.database.createUserApplicationForm(this.letters[this.section], this.appService.userFormId, newFormValues);
          }
        });
      });
    } else {
      this.router.params.pipe(
        map(url => {
          console.log(url);
          this.id = url.id;
          return url.id;
        }),
        switchMap(id => {
          console.log(id);
          return this.database.getUnauthApplicationForm();
        }),
        map((form: any) => {
          console.log(form);
          this.formValues = form.formValues;
          this.formOptions = form;
          this.formValuesNames = Object.keys(form.formValues);
          console.log(this.appService.form[this.letters[this.section]]);
          if (this.appService.form[this.letters[this.section]]) {
            this.formAnswers = this.appService.form[this.letters[this.section]].formValues;
            console.log(this.formAnswers);
          } else {
            for (const i of this.formValuesNames) {
              this.formAnswers[i] = '';
            }
          }
          return form.formValues;
        })
      ).subscribe(data => {
        console.log(data);
      });
    }

  }

  ngOnDestroy() {
    console.log('swtiching tabs');
    if (this.formValues) {
      this.save();
    }
  }

  next() {
    console.log('updating');
    const newFormValues = {formValues: {}};
    for (const i of this.formValuesNames) {
      newFormValues.formValues[i] = this.formAnswers[i];
    }
    this.appService.form[this.letters[this.section]] = newFormValues;
    console.log(this.appService.form['A']);
    // TODO: still need to add new route if more than one section
    this.router2.navigate(['/applications/' + this.id + '/apply/signup']);
  }

  save() {
    console.log('updating');
    const newFormValues = {formValues: {}};

    let address = '';
    if (this.line1) {address += this.line1 + '$'; }
    if (this.line2) {address += this.line2 + '$'; }
    if (this.city) {address += this.city + '$'; }
    if (this.province) {address += this.province + '$'; }

    for (const i of this.formValuesNames) {
      newFormValues.formValues[i] = this.formAnswers[i];
      if (i.indexOf('address') > -1) {
        newFormValues.formValues[i] = address;
      }
    }
    if (this.profile.userState.progress && this.profile.userState.progress !== 0) {
      this.database.updateApplicationForm(this.letters[this.section], this.appService.userFormId, newFormValues);
    }
    this.appService.form[this.letters[this.section]] = newFormValues;
  }

  submit() {
    this.save();
    this.router2.navigate(['/applications/' + this.id + '/apply/submit']);
  }

  sendEmail() {
    this.profile.emailVerification().then(email => email);
  }
}
