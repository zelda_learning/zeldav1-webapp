import { Injectable } from '@angular/core';
import { ProfileService} from './profile.service';
import { FirebaseIntegrationService } from './firebase-integration.service';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {
  appDocPromise;
  id;
  submitted ;
  userFormId;
  addForm;
  form : any = {}
  intro = false;
  unAuth = false;

  constructor(private profile : ProfileService, private database : FirebaseIntegrationService) {
  }

  init(id:string){
    this.id = id;
    this.appDocPromise = new Promise((resolve, reject) => {
      console.log(this.profile.userState, "User state in app");
      if((this.profile.userState.startedApplicationIds  && this.profile.userState.startedApplicationIds[this.id])||(this.profile.userState.completedApplicationIds&&this.profile.userState.completedApplicationIds[this.id])){
        var docId = ""
        if(this.profile.userState.startedApplicationIds && this.profile.userState.startedApplicationIds[this.id]){
          docId = this.profile.userState.startedApplicationIds[this.id];
        }
        else{
          docId = this.profile.userState.completedApplicationIds[this.id];
        }
        console.log("Document ID " +docId);
        this.database.getUserApplicationForm(docId).subscribe(data=>{
          console.log(data);
          if(data){
            console.log(data);
            this.submitted = data.submitted
            console.log(this.submitted);
            this.userFormId = docId;
            console.log(this.userFormId);
          }
          resolve(docId);
        })
      }
      else{
        if(this.profile.userState.progress && this.profile.userState.progress !=0){
          console.log('creating form ' +this.profile.uid +" "+this.id);
          this.database.createApplicationForm(this.profile.uid, this.id).then(data=>{
            console.log('form created' + data.id);
            this.userFormId = data.id;
          }).then(data=>{
            console.log('adding form to state');
            this.database.addFormToState(this.profile.uid,this.userFormId, this.id).then(data=>{
              console.log(data);
              this.addForm = data.subscribe(data=>{
                console.log(data);
                this.addForm.unsubscribe();
                resolve(this.userFormId);
              })
            });
          });
        }
        else{
          resolve(null);
        }
      }
    });
  }

  initUnauth(){
    this.unAuth = true;
    this.database.noMenu = true;
    this.id = '-GRRRRRL';
    console.log('creating form ' +this.profile.uid +" "+this.id);
    this.database.createApplicationForm(this.profile.uid, this.id).then(data=>{
      console.log('form created' + data.id);
      this.userFormId = data.id;
    }).then(data=>{
      console.log('adding form to state');
      this.database.addFormToState(this.profile.uid,this.userFormId, this.id).then(data=>{
        console.log(data);
        this.addForm = data.subscribe(data=>{
          console.log(data);
          this.addForm.unsubscribe();
        })
      });
    }).then(data=>{
      console.log(this.userFormId);
      console.log(this.form['A']);
      this.database.createUserApplicationForm('A', this.userFormId, this.form['A']);
    });
  }

  getAppDocID(){
    return this.appDocPromise;
  }
}
