import { Injectable } from '@angular/core';
import { FirebaseIntegrationService } from './firebase-integration.service';
import { Router } from '@angular/router';
import {map, catchError} from 'rxjs/operators';
import {AuthService} from './auth.service';

@Injectable()
export class ProfileService {

  userState: any;
  userPersonal: any;
  userSchool: any;
  userPersonality: any;
  imageName: string;
  route = '';
  temp: string;
  pic: string;
  picPath: string;
  uid: string;
  marks: Array<any> = [];
  keys: Array<any> = [];
  numSub = 0;
  menClass = 'col-xl-2 col-lg-3 col-md-4 sidebar fixed-top active';
  menu = true;
  hClass = 'col-xl-10 ml-auto fixed-top py-0 top-navbar';
  line1 = '';
  line2 = '';
  city = '';
  province = '';
  zip = '';
  sline1 = '';
  sline2 = '';
  scity = '';
  sprovince = '';
  szip = '';
  user: any = {};
  messages = false;

  constructor(private  database: FirebaseIntegrationService, private router: Router, private auth: AuthService) {
    this.pic = '';

    this.userPersonal = {
      id : '',
      address : '',
      altContactNumber : '',
      altContactRelationship : '',
      awards : '',
      careerIntentions : '',
      combinedYearlyIncome : '',
      contactNumber : '',
      culturalActivities: '',
      crimes: '',
      dateOfBirth: '',
      disability: '',
      email: '',
      firstName: '',
      gender: 'Prefer not to say',
      guardianOccupation: '',
      hobbies: '',
      homeLanguage: '',
      homeOrigin: '',
      idUploaded: '',
      idNumber: '',
      lastName: '',
      maritalStatus: '',
      nationality: '',
      placeOfBirth: '',
      otherLanguage: '',
      race: 'Prefer not to say',
      saCitizen: ''
    };

    this.userSchool = {
      id : '',
      currentGrade : '',
      matricCertificateUploaded : '',
      matricCompletedYear: '',
      subjectMarksMostRecent : {},
      schoolName: '',
      schoolYearEnded: '',
      schoolYearStarted: '',
      schoolAddress: '',
      schoolReportUploaded: '',
      universityYear: '',
      universityName: '',
      degreeName: '',
      degreeStartedYear: '',
      degreeApplicationYear: '',
      degreeDuration: '',
      postMatricStudies: ''
    };

    this.userState = {
      interests: '',
      username: '',
      fieldPreference : {},
      pinnedBursaries : []
    };

    this.userPersonality = {};

    this.database.getUserPersonal().then(userData => {
      userData.subscribe( data => {
        if (data) {
          this.userPersonal = {...this.userPersonal, ...data};
          const pieces = data.address.split('$');
          this.line1 = pieces[0];
          if (pieces[1]) {
            this.line2 = pieces[1];
          }
          if (pieces[2]) {
            this.city = pieces[2];
          }
          if (pieces[3]) {
            this.province = pieces[3];
          }
          if (pieces[4]) {
            this.zip = pieces[4];
          }
        }
      });
    });

    this.database.getUserState().then(userState => {
      userState.subscribe(data => {
        if (data) {
          this.userState = data;
        }
      });
    });

    this.database.getUID().then(uid => {
      if (uid) {
        this.uid = uid;
        console.log(this.uid);
        this.database.getDownloadUrl('UserStates/' + uid + '.jpg').pipe(
          map(uid => uid),
          catchError(err => {
            return this.database.getDownloadUrl('UserStates/' + uid + '.png');
          }),
          map(uid => uid),
          catchError(err => {
            return this.database.getDownloadUrl('UserStates/1.jpg');
          }),
          map(url => url)
        ).subscribe(url => {
          this.pic = url;
        });
      }
    });

    this.database.getUserSchool().then(sub => {
      sub.subscribe(data => {
          if (data && data.subjectMarksMostRecent) {
              this.userSchool = {...this.userSchool, ...data};
              this.keys = Object.keys(data.subjectMarksMostRecent);
              const pieces = data.schoolAddress.split('$');
              this.sline1 = pieces[0];
              if (pieces[1]) {
                this.sline2 = pieces[1];
              }
              if (pieces[2]) {
                this.scity = pieces[2];
              }
              if (pieces[3]) {
                this.sprovince = pieces[3];
              }
              if (pieces[4]) {
                this.szip = pieces[4];
              }
          } else {
            this.keys = [];
          }

          this.numSub = this.keys.length;
          for ( const sub2 of this.keys) {
            this.marks.push(this.userSchool.subjectMarksMostRecent[sub2]);
          }
      });
    });

    this.database.getUserPersonality().then(data => {
      data.subscribe(data => {
        if (data) {
          this.userPersonality = data;
        }
      });
    });
  }

  saveRes(path, name) {
    return this.database.saveRes(path, name).then(data => data);
  }

  addSubject() {
    this.numSub++;
    this.keys.push('Subject ' + this.numSub );
    this.marks.push(0);
  }

  removeSubject() {
    if (this.numSub > 0) {
      this.numSub--;
      this.keys.splice(this.numSub, 1);
      this.marks.splice(this.numSub, 1);
    }
  }

  update() {
    if (this.userSchool.yearOfMatric ) {
      this.userSchool.schoolYearComplete = this.userSchool.yearOfMatric;
    }
    let address = '';
    if (this.line1) {address += this.line1 + '$'; }
    if (this.line2) {address += this.line2 + '$'; }
    if (this.city) {address += this.city + '$'; }
    if (this.province) {address += this.province + '$'; }
    if (this.zip) {address += this.zip; }
    let saddress = '';
    if (this.sline1) {saddress += this.sline1 + '$'; }
    if (this.sline2) {saddress += this.sline2 + '$'; }
    if (this.scity) {saddress += this.scity + '$'; }
    if (this.sprovince) {saddress += this.sprovince + '$'; }
    if (this.szip) {saddress += this.szip; }
    this.userPersonal.address = address;
    this.userSchool.schoolAddress = saddress;
    this.userSchool.subjectMarksMostRecent = {};
    this.userState.progress = this.getProgress();
    for (let i = 0; i < this.numSub; i++) {
      this.userSchool.subjectMarksMostRecent[this.keys[i]] = this.marks[i];
    }
    this.database.updateUserPersonal(this.userPersonal);
    this.database.updateUserState(this.userState);
    this.database.updateUserSchool(this.userSchool);
  }

  getInterests() {
    let interests = '';
    const intlist = Object.keys(this.userState.fieldPreference);
    for (let i = 0; i < intlist.length; i++) {
      interests = interests + intlist[i] + ', ';
    }
    interests = interests.substr(0, interests.length - 2);
    return interests;
  }

  isFavourite(fav: string) {
    if (this.userState.fieldPreference[fav]) {
      return true;
    }
    return false;
  }

  addFav(fav: string) {
    this.userState.fieldPreference[fav] = true;
    this.database.updateUserState(this.userState);
  }

  removeFav(fav: string) {
    delete this.userState.fieldPreference[fav];
    this.database.updateUserState(this.userState);
  }

  getUserPersonality() {
    return this.userPersonality;
  }

  emailVerification() {
    return this.database.getUID().then(uid => {
      return this.auth.getCurrentUser().sendEmailVerification();
    });
  }

  getUserVerified() {
    return this.database.getUID().then(uid => {
      const user =  this.auth.getCurrentUser();
      console.log(user.providerData['0']);
      if (user.providerData['0'].providerId === 'password') {
        return user.emailVerified;
      } else {
        return true;
      }
    });
  }

  getCurrentUser() {
    return this.auth.getCurrentUser();
  }

  changeEmail(email: string) {
    return this.auth.updateEmail(email);
  }

  forgotPassword(email: string) {
    return this.auth.resetPassword(email);
  }

  getProgress() {
    if (!this.userState.optIn) { return 0; }
    const ppKeys = Object.keys(this.userPersonal);
    const schKeys = Object.keys(this.userSchool);
    if ( this.userSchool.currentGrade !== 'Matriculated') {
      this.userSchool.postMatricStudies = 'N/A';
      this.userSchool.matricCertificateUploaded = 'N/A';
      this.userSchool.schoolYearEnded = 'N/A';
    } else { this.userSchool.schoolReportUploaded = 'N/A'; }

    if (this.userSchool.universityYear === 'Not currently at university') {
      this.userSchool.degreeStartedYear = 'N/A';
      this.userSchool.degreeApplicationYear = 'N/A';
      this.userSchool.degreeDuration = 'N/A';
    }
    let numAns = 0;
    for (const i of ppKeys) {
      if (this.userPersonal[i] && this.userPersonal[i] !== '') {
        numAns++;
      } else {
        console.log(i);
      }
    }
    for (const i of schKeys) {
      if (this.userSchool[i] && this.userSchool[i] !== '') {
        numAns++;
      } else {
        console.log(i);
      }
    }

    if (this.userState.fieldPreference) {
      const intKeys = Object.keys(this.userState.fieldPreference);
      if (intKeys.length > 1) { numAns++; }
    }

    const totalAns = ppKeys.length + schKeys.length + 1 ;
    const toRet = Math.round( numAns / totalAns * 100 ) ;
    if (toRet > 100) { return 100; } else { return toRet; }
  }

  updateUserState() {
    this.database.updateUserState(this.userState);
  }
}
