import { Component, ViewChild, Input, OnInit, HostListener } from '@angular/core';
import {LoginComponent} from './/login/login.component';
import {FirebaseIntegrationService} from './/firebase-integration.service';
import {ProfileService} from './/profile.service';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private router: Router, public database: FirebaseIntegrationService, public profile: ProfileService) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        (<any>window).ga('set', 'page', event.urlAfterRedirects);
        (<any>window).ga('send', 'pageview');
      }
    });
  }

  @HostListener('window:beforeunload', ['$event']) beforeunloadHandler(event) {
    this.Destroy();
  }

  ngOnInit() {
    if (screen.width < 1200) {
      console.log(screen.width + " is too low res");
      this.profile.menClass = "col-xl-2 col-lg-3 col-md-4 sidebar fixed-top notactive";
      this.profile.menu = false;
    }
    this.database.getUserPersonal().then(data => {
      if(data && !this.database.noMenu) {
        this.database.rClass = "col-md-10 ml-auto margin";
      }
    });
  }

  Destroy(): void {
    if (this.profile.userState.progress && this.profile.userState.progress !== 100) {
      (<any>window).ga('send', 'event', {
        eventCategory: 'UserProgress',
        eventLabel: '',
        eventAction: 'ProgressOnExit',
        eventValue: this.profile.userState.progress
      });
    }
  }
}
