import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import {FIELDS} from '../objects/fields';

@Component({
  selector: 'app-sign-up-chat',
  templateUrl: './sign-up-chat.component.html',
  styleUrls: ['./sign-up-chat.component.scss']
})
export class SignUpChatComponent implements OnInit {
  stage = 1;
  def = ["Hey, welcome to Zelda Learning", "Are you ready to start your Zelda journey? We'd like to ask a few things so we can get to know you and personalise your experience."];
  numint = 0;
  fields = FIELDS;
  more = false ;

  constructor(public profile: ProfileService, private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.database.getUID().then(uid=>{
      console.log(uid);
      this.database.createUserState(uid).then(data=>{
          this.database.createUserPersonal(uid);
          this.database.createUserSchool(uid);
          this.profile.emailVerification();
      });
    });
  }

  setStage(stage:number){
    this.stage = stage;
    this.scrollTop();
    this.profile.update();
  }

  scrollTop(){
    /*var objDiv = document.getElementById("window");
    var height = objDiv.scrollHeight;
    window.scrollTo(0,height);*/
  }

  contains(field: string){
    this.scrollTop();
    if(this.profile.userState.fieldPreference[field]){
      return true;
    }
    else
      return false;
  }

  checkEvent(e) {
    if (!this.profile.userState.fieldPreference[e]) {
      this.profile.userState.fieldPreference[e] = true;
      this.numint++;
    } else {
      delete this.profile.userState.fieldPreference[e];
      this.numint--;
    }
  }

  optIn() {
    this.database.createCBAForm().then(data=>{
      this.database.addCbaToState(data.id).then(
        data2 => {
          this.sendOptInChat();
          console.log(data2);
        }
        );
    });
  }

  sendOptInChat = () => {
    (<any>window).ga('send', 'event', {
      eventCategory: 'OptIn',
      eventLabel: '',
      eventAction: 'OptInChat',
      eventValue: 1
    });
  }
}
