import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpChatComponent } from './sign-up-chat.component';

describe('SignUpChatComponent', () => {
  let component: SignUpChatComponent;
  let fixture: ComponentFixture<SignUpChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
