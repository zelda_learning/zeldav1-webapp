import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import {FirebaseIntegrationService} from '../firebase-integration.service';
@Component({
  selector: 'app-profile-personal-info',
  templateUrl: './profile-personal-info.component.html',
  styleUrls: ['./profile-personal-info.component.scss']
})
export class ProfilePersonalInfoComponent implements OnInit {
  idUploaded = false;
  idDownloadUrl = '';

  constructor(public  profile: ProfileService, private database: FirebaseIntegrationService) {
   }

  ngOnInit() {
    console.log(this.profile.userPersonal);
    if (this.profile.userPersonal.idUploaded && this.profile.userPersonal.idUploaded !== '') {
      this.idUploaded = true;
      const ext = this.profile.userPersonal.idUploaded.substr
      (this.profile.userPersonal.idUploaded.indexOf('.'), this.profile.userPersonal.idUploaded.length);
      if (ext === '.pdf') {
        this.database.getDownloadUrl('/UserProfile_pdf/idCopy/' + this.profile.userPersonal.idUploaded).subscribe(data2 => {
          this.idDownloadUrl = data2 ;
        });
      } else {
        this.database.getDownloadUrl('UserProfile_image/idCopy/' + this.profile.userPersonal.idUploaded).subscribe(data2 => {
          this.idDownloadUrl = data2 ;
        });
      }
    }
  }

  upID(event) {
     console.log( event.target.files[0]);

     let imageName = '';
     let fileName = '';
     if (event.target.files[0].type === 'application/pdf') {
       imageName = '/UserProfile_pdf/idCopy/';
       fileName = this.profile.uid + '.pdf';
     } else {
       imageName = '/UserProfile_image/idCopy/' ;
       fileName = this.profile.uid + '.jpg';
     }

     this.profile.saveRes(event.target.files[0], imageName + fileName).then(data => {
       this.profile.userPersonal.idUploaded = fileName;
       this.idUploaded = true;
       const ext = this.profile.userPersonal.idUploaded.substr
       (this.profile.userPersonal.idUploaded.indexOf('.'), this.profile.userPersonal.idUploaded.length);
       if (ext === '.pdf') {
         this.database.getDownloadUrl('/UserProfile_pdf/idCopy/' + this.profile.userPersonal.idUploaded).subscribe(data2 => {
           this.idDownloadUrl = data2 ;
         });
       } else {
         this.database.getDownloadUrl('UserProfile_image/idCopy/' + this.profile.userPersonal.idUploaded).subscribe(data2 => {
           this.idDownloadUrl = data2 ;
         });
       }
     });
  }

  removeId() {
    delete this.profile.userPersonal.idUploaded;
    this.idUploaded = false;
  }

  goToLink(url: string) {
      window.open(url, '_blank');
  }

  getPersonalClass(attr: string) {
    return {
      'is-valid' : this.profile.userPersonal[attr] ,
      'is-invalid' : !this.profile.userPersonal[attr]
    };
  }

  getProfileClass(attr: string) {
    return {
      'is-valid' : this.profile[attr] ,
      'is-invalid' : !this.profile[attr]
    };
  }
}
