import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import {FIELDS} from '../objects/fields';

@Component({
  selector: 'app-profile-activities',
  templateUrl: './profile-activities.component.html',
  styleUrls: ['./profile-activities.component.scss']
})
export class ProfileActivitiesComponent implements OnInit {
  picPath: string = "";
  verified = true;
  changingEmail = false;
  email: string;
  error: string;
  emailEnabled: boolean;
  emailSent = false;
  fields = FIELDS;

  constructor(public  profile : ProfileService) { }

  ngOnInit() {
    this.email = this.profile.getCurrentUser().email;
    if (this.email) {
      this.emailEnabled = true;
    }
    this.profile.getUserVerified().then(verified => { this.verified = verified; });
  }

  upPic(event){
    this.picPath = event.target.files[0];
    var imageName = "/UserStates/"+this.profile.uid+".jpg";
    this.profile.saveRes(this.picPath, imageName);
  }

  getNumKeys () {

    if (this.profile.userState.fieldPreference
      && Object.keys(this.profile.userState.fieldPreference)
      && Object.keys(this.profile.userState.fieldPreference).length > 1) {
       return false;
    } else  { return true; }
  }

  contains(field: string){
    if(this.profile.userState.fieldPreference[field]){
      return true;
    }
    else
      return false
  }

  checkEvent(e) {
    if (!this.profile.userState.fieldPreference[e]) {
      this.profile.userState.fieldPreference[e] = true;
    } else {
      delete this.profile.userState.fieldPreference[e];
    }
  }

  verifyEmail() {
    this.profile.emailVerification();
    this.emailSent = true;
  }

  changeEmail() {
    this.profile.changeEmail(this.email).then(
      () => {
        this.changingEmail = false;
        this.error = null;
      },
      err => {
        this.error = 'You need to have logged in recently to change your email address' +
          ' or the email address is not correct. Please logout and then login again to change your email';
      }
    );
  }
}
