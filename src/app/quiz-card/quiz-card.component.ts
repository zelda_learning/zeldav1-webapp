import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../profile.service';

@Component({
  selector: 'app-quiz-card',
  templateUrl: './quiz-card.component.html',
  styleUrls: ['./quiz-card.component.scss']
})
export class QuizCardComponent implements OnInit {
  quizzes : any = {};
  numQuiz :number = 0;
  constructor(private profile : ProfileService) { }

  ngOnInit() {
    this.quizzes = this.profile.userState.pinnedBursaries;
  }

}
