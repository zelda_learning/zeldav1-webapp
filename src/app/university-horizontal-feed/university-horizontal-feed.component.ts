import { Component, OnInit } from '@angular/core';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { University } from '../objects/university';

@Component({
  selector: 'app-university-horizontal-feed',
  templateUrl: './university-horizontal-feed.component.html',
  styleUrls: ['./university-horizontal-feed.component.scss']
})
export class UniversityHorizontalFeedComponent implements OnInit {
  universities : University[];

  constructor(private database: FirebaseIntegrationService) { }

  ngOnInit() {
    this.database.getUniversities().subscribe(data =>{
      this.universities= data;
      });
  }

}
