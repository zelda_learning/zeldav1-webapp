import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniversityHorizontalFeedComponent } from './university-horizontal-feed.component';

describe('UniversityHorizontalFeedComponent', () => {
  let component: UniversityHorizontalFeedComponent;
  let fixture: ComponentFixture<UniversityHorizontalFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversityHorizontalFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniversityHorizontalFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
