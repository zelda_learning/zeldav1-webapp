import {UserProfileFETFutureInterface} from '../objects/userProfileFETFuture.interface';
import {University} from '../objects/university';
import {UserProfileFET} from '../objects/userProfileFET';

export class UserProfileFETFuture extends UserProfileFET{
  universityPreferenceList : University[];
  degreePreferenceList : String[];
  constructor(objIn: UserProfileFETFutureInterface){
    super(objIn);
    this.universityPreferenceList = objIn.universityPreferenceList;
    this.degreePreferenceList = objIn.degreePreferenceList;
  }
}
