import {RootObjectInterface} from '../objects/rootObject.interface';

export interface CompanyInterface extends RootObjectInterface{
  link: String;
  text: String;
  blurb: String;
}
