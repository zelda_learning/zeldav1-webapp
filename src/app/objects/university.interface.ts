import {ResourceObjectInterface} from '../objects/resourceObject.interface';

export interface UniversityInterface extends ResourceObjectInterface {
  location: String;
  applicationOpenDate: String;
  link : String;
}
