import {ResourceObjectInterface} from '../objects/resourceObject.interface';

export interface ApplicationInterface extends ResourceObjectInterface{
  companyName : String;
  companyId : String;
  dateOpen : String;
  dateClosed : String;
}
