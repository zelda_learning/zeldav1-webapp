import {ResourceObject} from '../objects/resourceObject';
import {ApplicationInterface} from '../objects/application.interface';

export class Application extends ResourceObject{
  companyName : String;
  companyId : String;
  dateOpen : String;
  dateClosed : String;
  constructor(objIn : ApplicationInterface){
    super(objIn);
    this.companyName = objIn.companyName;
    this.companyId = objIn.companyId;
    this.dateOpen = objIn.dateOpen;
    this.dateClosed = objIn.dateClosed;
  }
}
