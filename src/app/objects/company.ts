import {RootObject} from '../objects/rootObject';
import {CompanyInterface} from '../objects/company.interface';

export class Company extends RootObject{
  link: String;
  text: String;
  blurb: String;
  constructor(objIn : CompanyInterface){
    super(objIn);
    this.link = objIn.link;
    this.text = objIn.text;
    this.blurb = objIn.blurb;
  }
}
