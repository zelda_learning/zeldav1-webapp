import {UserProfileInterface} from '../objects/userProfile.interface';

export interface UserProfileFETInterface extends UserProfileInterface{
  bursaryFunding : String;
  nsafsFunding : String;
}
