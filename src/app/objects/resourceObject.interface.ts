import {RootObjectInterface} from '../objects/rootObject.interface';
export interface ResourceObjectInterface  extends RootObjectInterface{
  subtitle: String;
  text: String;
  field: String;
}
