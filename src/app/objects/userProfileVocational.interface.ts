import {UserProfileFETInterface} from '../objects/userProfileFET.interface';

export interface UserProfileVocationalInterface extends UserProfileFETInterface{
  highestQualification : String;
  skillsTraining : String;
}
