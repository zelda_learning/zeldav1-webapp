import {RootObjectInterface} from '../objects/rootObject.interface';
import {Subject} from '../objects/subject';

export interface UserProfileInterface extends RootObjectInterface{
  age: number;
  progress: number;
  numberElements: number;
  citizenship: String;
  currentEnrollmentStatus: String;
  emailAddress: String;
  firstName: String;
  gender: String;
  idNumber: String;
  lastName: String;
  matricSubjects: Subject[];
  matricYear: String;
  nationality: String;
  phoneNumber: String;
  race: String;
  schoolCountry: String;
  schoolName: String;
}
