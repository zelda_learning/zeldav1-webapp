import {UserProfileVocationalInterface} from '../objects/userProfileVocational.interface';
import {UserProfileFET} from '../objects/userProfileFET';

export class UserProfileVocational extends UserProfileFET{
  highestQualification : String;
  skillsTraining : String;
  constructor(objIn : UserProfileVocationalInterface){
    super(objIn);
    this.highestQualification = objIn.highestQualification;
    this.skillsTraining = objIn.skillsTraining;
  }
}
