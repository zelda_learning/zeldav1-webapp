import {MenuItem} from '../objects/menuItem';

export const MENUDATA : MenuItem[]= [
  {id:1, imageName:"fa fa-home text-light fa-lg mr-2", text:"Home", ref:"/home"},
  {id:2, imageName:"fa fa-graduation-cap text-light fa-lg mr-2", text:"Bursaries", ref:"/applications"},
  {id:3, imageName:"fa fa-university text-light fa-lg mr-2", text:"Studies", ref:"/universities"},
  {id:4, imageName:"fa fa-clipboard text-light fa-lg mr-2", text:"NBTs", ref:"/quizzes"},
  {id:5, imageName:"fa fa-book text-light fa-lg mr-2", text:"Tips", ref:"/articles"}
];
