import {RootObject} from '../objects/rootObject';
import {ResourceObjectInterface} from '../objects/resourceObject.interface';

export class ResourceObject extends RootObject{
  subtitle: String;
  text: String;
  field: String;
  constructor(objIn : ResourceObjectInterface){
    super(objIn);
    this.subtitle = objIn.subtitle;
    this.text = objIn.text;
    this.field = objIn.field;
  }
}
