import {ResourceObject} from '../objects/resourceObject';
import {UniversityInterface} from '../objects/university.interface';

export class University extends ResourceObject{
  location: String;
  applicationOpenDate: String;
  link : String;
  constructor (objIn : UniversityInterface){
    super(objIn);
    this.location = objIn.location;
    this.applicationOpenDate = objIn.applicationOpenDate;
    this.link = objIn.link;
  }
}
