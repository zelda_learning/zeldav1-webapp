import {UserProfile} from '../objects/userProfile';
import {UserProfileFETInterface} from '../objects/userProfileFET.interface';

export class UserProfileFET extends UserProfile{
  bursaryFunding : String;
  nsafsFunding : String;
    constructor(objIn : UserProfileFETInterface){
      super(objIn);
      this.bursaryFunding = objIn.bursaryFunding;
      this.nsafsFunding = objIn.nsafsFunding;
    }
}
