export class Phone {
  country: string;
  area?: string;
  prefix?: string;
  line: string;

  // format phone numbers as E.164
  get e164() {
    if(this.line && this.line[0] == '0'){
      this.line = this.line.substr(1);
    }
    this.line = this.line.replace(/ /g, "");
    const num = this.country + this.line
    return `${num}`
  }

}
