import {UserProfileFETFutureInterface} from '../objects/userProfileFETFuture.interface';
import {University} from '../objects/university';
import {UserProfileFET} from '../objects/userProfileFET';

export class UserProfileFETCurrent extends UserProfileFET{
  constructor(objIn: UserProfileFETFutureInterface){
    super(objIn);

  }
}
