import {ResourceObject} from '../objects/resourceObject';
import {ArticleInterface} from '../objects/article.interface';

export class Article extends ResourceObject{
  companyName : String;
  companyId : String;
  datePosted : String;
  constructor(objIn: ArticleInterface){
    super(objIn);
    this.companyName = objIn.companyName;
    this.companyId = objIn.companyId;
    this.datePosted = objIn.datePosted;
  }
}
