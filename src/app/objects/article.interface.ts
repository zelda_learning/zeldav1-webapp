import {ResourceObjectInterface} from  '../objects/resourceObject.interface';

export interface ArticleInterface  extends ResourceObjectInterface{
  companyName : String;
  companyId : String;
  datePosted : String;
}
