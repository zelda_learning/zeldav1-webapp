import {RootObjectInterface} from '../objects/rootObject.interface';

export class RootObject{
  id : String;
  title : String;
  imageName : String;
  constructor(objIn : RootObjectInterface){
    this.id = objIn.id;
    this.title = objIn.title;
    this.imageName = objIn.imageName;
  }
}
