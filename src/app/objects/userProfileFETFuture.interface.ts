import {UserProfileFETInterface} from '../objects/userProfileFET.interface';
import {University} from '../objects/university';

export interface UserProfileFETFutureInterface extends UserProfileFETInterface{
  universityPreferenceList : University[];
  degreePreferenceList : String[];
}
