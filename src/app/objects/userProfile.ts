import {RootObject} from '../objects/rootObject';
import {UserProfileInterface} from '../objects/userProfile.interface';
import {Subject} from '../objects/subject';

export class UserProfile extends RootObject{
  age: number;
  progress: number;
  numberElements: number;
  citizenship: String;
  currentEnrollmentStatus: String;
  emailAddress: String;
  firstName: String;
  gender: String;
  idNumber: String;
  lastName: String;
  matricSubjects: Subject[];
  matricYear: String;
  nationality: String;
  phoneNumber: String;
  race: String;
  schoolCountry: String;
  schoolName: String;
  constructor(objIn: UserProfileInterface){
    super(objIn);
    this.age = objIn.age;
    this.progress = objIn.progress;
    this.numberElements = objIn.numberElements;
    this.citizenship = objIn.citizenship;
    this.currentEnrollmentStatus = objIn.currentEnrollmentStatus;
    this.emailAddress = objIn.emailAddress;
    this.firstName = objIn.firstName;
    this.gender = objIn.gender;
    this.idNumber = objIn.idNumber;
    this.lastName = objIn.lastName;
    this.matricYear = objIn.matricYear;
    this.nationality = objIn.nationality;
    this.phoneNumber = objIn.phoneNumber;
    this.race = objIn.race;
    this.schoolCountry = objIn.schoolCountry;
    this.schoolName = objIn.schoolName;
    this.matricSubjects = objIn.matricSubjects;
  }
}
