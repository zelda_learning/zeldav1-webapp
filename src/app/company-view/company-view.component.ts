import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-company-view',
  templateUrl: './company-view.component.html',
  styleUrls: ['./company-view.component.scss']
})
export class CompanyViewComponent implements OnInit {
  company :any;
  pic: string;
  id: string;

  constructor(private route : ActivatedRoute ,
    private database : FirebaseIntegrationService,
    private router: Router) { }

  ngOnInit() {
      this.company ={};
      this.pic = "";
      this.route.params.subscribe(args =>{
        this.id = args.id;
        this.database.getCompany(this.id).subscribe(data =>{
          this.company = data;
        });

        this.database.getDownloadUrl("/Companies/"+this.id +".jpg").subscribe(data =>{
          this.pic = data;
        });
      });
  }

}
