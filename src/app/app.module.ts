// angular imports
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartModule } from 'angular2-chartjs';

// angularfire and firebase imports
import {AngularFireModule} from '@angular/fire';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireDatabase} from '@angular/fire/database';
import {environment} from 'environments/environment';
import {AuthService} from './/auth.service';
import {FirebaseIntegrationService} from './/firebase-integration.service';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireMessagingModule} from '@angular/fire/messaging';

import { AppComponent } from './app.component';
import { UserStateComponent } from './user-state/user-state.component';
import { ApplicationComponent } from './application/application.component';
import { UniversityComponent } from './university/university.component';
import { CompanyComponent } from './company/company.component';
import { QuizComponent } from './quiz/quiz.component';
import { ArticleComponent } from './article/article.component';
import { MenuComponent } from './menu/menu.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './home/home.component';
import { FeedComponent } from './feed/feed.component';
import { ProfileComponent } from './profile/profile.component';
import { ApplicationItemComponent } from './application-item/application-item.component';
import { UniversityItemComponent } from './university-item/university-item.component';
import { CompanyItemComponent } from './company-item/company-item.component';
import { UserProgressComponent } from './user-progress/user-progress.component';
import { PersonalityComponent } from './personality/personality.component';
import { RecommendedComponent } from './recommended/recommended.component';
import { QuizItemComponent } from './quiz-item/quiz-item.component';
import { ArticleItemComponent } from './article-item/article-item.component';
import { FeedItemComponent } from './feed-item/feed-item.component';
import { ArticleViewComponent } from './article-view/article-view.component';
import { CompanyViewComponent } from './company-view/company-view.component';
import { UniversityViewComponent } from './university-view/university-view.component';
import { TipsComponent } from './tips/tips.component';
import { ApplicationHorizontalFeedComponent } from './application-horizontal-feed/application-horizontal-feed.component';
import { ApplicationFeedItemComponent } from './application-feed-item/application-feed-item.component';
import { UniversityFeedItemComponent } from './university-feed-item/university-feed-item.component';
import { UniversityHorizontalFeedComponent } from './university-horizontal-feed/university-horizontal-feed.component';
import { ApplicationViewComponent } from './application-view/application-view.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { QuizViewComponent } from './quiz-view/quiz-view.component';
import { QuestionComponent } from './question/question.component';
import { CheckAnswersComponent } from './check-answers/check-answers.component';
import { QuizAnswerService } from './quiz-answer.service';
import { FavouriteService} from './favourite.service';
import { ProfileService} from './profile.service';
import { FavouritesCardComponent} from './favourites-card/favourites-card.component';
import { QuizCardComponent} from './quiz-card/quiz-card.component';
import { BursaryTableComponent } from './bursary-table/bursary-table.component';
import { ProfileActivitiesComponent } from './profile-activities/profile-activities.component';
import { ProfilePersonalInfoComponent } from './profile-personal-info/profile-personal-info.component';
import { ProfileSchoolInfoComponent } from './profile-school-info/profile-school-info.component';
import { BursaryRecommendedTableComponent } from './bursary-recommended-table/bursary-recommended-table.component';
import { SignUpChatComponent } from './sign-up-chat/sign-up-chat.component';
import { IntroQuizComponent } from './intro-quiz/intro-quiz.component';
import { TermsComponent } from './terms/terms.component';
import { FaqComponent } from './faq/faq.component';
import * as firebase from 'firebase/app';
import { IntroQuizCardComponent } from './intro-quiz-card/intro-quiz-card.component';
import { PersonalityCardComponent } from './personality-card/personality-card.component';
import { HumanitiesComponent } from './fields/humanities/humanities.component';
import { HealthScienceComponent } from './fields/health-science/health-science.component';
import { LawComponent } from './fields/law/law.component';
import { ScienceComponent } from './fields/science/science.component';
import { CommerceComponent } from './fields/commerce/commerce.component';
import { EbeComponent } from './fields/ebe/ebe.component';
import { ApplicationFormComponent } from './application-form/application-form.component';
import { ApplicationFormFragmentComponent } from './application-form-fragment/application-form-fragment.component';
import { ApplicationSubmitComponent } from './application-submit/application-submit.component';
import { ApplicationSubmitFragmentComponent } from './application-submit-fragment/application-submit-fragment.component';
import { ApplicationSubmitIntroComponent } from './application-submit-intro/application-submit-intro.component';
import { ApplicationService } from './application.service';
import { UserApplicationCardComponent } from './user-application-card/user-application-card.component';
import { UserApplicationsComponent } from './user-applications/user-applications.component';
import { MessagingComponent } from './messaging/messaging.component';
import { NotificationsService } from './notifications.service';
import { PhoneLoginComponent } from './phone-login/phone-login.component';
import { WindowService } from './window.service';
import { LoginEmailComponent } from './login-email/login-email.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import {RouteReuseStrategy} from '@angular/router';
import {CustomReuseStrategy} from './router.strat';
import { ApplicationSearchPipe } from './application/application-search.pipe';
import { OptInCardComponent } from './opt-in-card/opt-in-card.component';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { SchoolCardComponent } from './school-card/school-card.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HeaderComponent,
    AppComponent,
    UserStateComponent,
    ApplicationComponent,
    UniversityComponent,
    CompanyComponent,
    QuizComponent,
    ArticleComponent,
    HomeComponent,
    FeedComponent,
    ProfileComponent,
    ApplicationItemComponent,
    UniversityItemComponent,
    CompanyItemComponent,
    UserProgressComponent,
    PersonalityComponent,
    RecommendedComponent,
    QuizItemComponent,
    ArticleItemComponent,
    FeedItemComponent,
    ArticleViewComponent,
    CompanyViewComponent,
    UniversityViewComponent,
    TipsComponent,
    ApplicationHorizontalFeedComponent,
    ApplicationFeedItemComponent,
    UniversityFeedItemComponent,
    UniversityHorizontalFeedComponent,
    ApplicationViewComponent,
    LoginComponent,
    SignupComponent,
    QuizViewComponent,
    QuestionComponent,
    CheckAnswersComponent,
    QuizCardComponent,
    FavouritesCardComponent,
    BursaryTableComponent,
    ProfileActivitiesComponent,
    ProfilePersonalInfoComponent,
    ProfileSchoolInfoComponent,
    BursaryRecommendedTableComponent,
    SignUpChatComponent,
    IntroQuizComponent,
    TermsComponent,
    FaqComponent,
    IntroQuizCardComponent,
    PersonalityCardComponent,
    HumanitiesComponent,
    HealthScienceComponent,
    LawComponent,
    ScienceComponent,
    CommerceComponent,
    EbeComponent,
    ApplicationFormComponent,
    ApplicationFormFragmentComponent,
    ApplicationSubmitComponent,
    ApplicationSubmitFragmentComponent,
    ApplicationSubmitIntroComponent,
    UserApplicationCardComponent,
    UserApplicationsComponent,
    MessagingComponent,
    PhoneLoginComponent,
    LoginEmailComponent,
    ThankyouComponent,
    ApplicationSearchPipe,
    OptInCardComponent,
    SchoolCardComponent
  ],
  imports: [
    FlashMessagesModule.forRoot(),
    ChartModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase, 'zeldav1'),
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    AngularFirestoreModule,
    AngularFireMessagingModule
  ],
  providers: [ AuthService, FirebaseIntegrationService,
    AngularFireDatabase, AngularFireAuth, QuizAnswerService, FavouriteService,
    ProfileService, ApplicationService, NotificationsService, WindowService ,
    {provide: RouteReuseStrategy, useClass: CustomReuseStrategy}],

  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    firebase.initializeApp(environment.firebase);
  }
}
