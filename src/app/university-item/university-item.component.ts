import { Component, OnInit, Input } from '@angular/core';
import { University } from '../objects/university';
import { FirebaseIntegrationService } from '../firebase-integration.service';

@Component({
  selector: 'app-university-item',
  templateUrl: './university-item.component.html',
  styleUrls: ['./university-item.component.scss']
})
export class UniversityItemComponent implements OnInit {
  @Input() item : any;
  pic: String;
  cont: String;
  date : String;
  constructor(private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.pic = "";
    this.cont = this.item.text.replace(/<\/?[^>]+(>|$)/g, "");
    this.database.getDownloadUrl("/Universities/" + this.item.id + ".jpg").subscribe(url=>{
      this.pic = url;
    });
  }

}
