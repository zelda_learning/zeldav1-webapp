import { Component, OnInit } from '@angular/core';
import {ProfileService} from '../profile.service';

@Component({
  selector: 'app-school-card',
  templateUrl: './school-card.component.html',
  styleUrls: ['./school-card.component.scss']
})
export class SchoolCardComponent implements OnInit {
  schoolName: string;

  constructor(public profile: ProfileService) { }

  ngOnInit() {
  }

  submit() {
    if (this.schoolName && this.schoolName !== '') {
      this.profile.userState.schoolName = this.schoolName;
      this.profile.updateUserState();
    }
  }

  close() {
    this.profile.userState.schoolName = 'N/A';
    this.profile.updateUserState();
  }
}
