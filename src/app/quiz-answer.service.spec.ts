import { TestBed, inject } from '@angular/core/testing';

import { QuizAnswerService } from './quiz-answer.service';

describe('QuizAnswerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuizAnswerService]
    });
  });

  it('should be created', inject([QuizAnswerService], (service: QuizAnswerService) => {
    expect(service).toBeTruthy();
  }));
});
