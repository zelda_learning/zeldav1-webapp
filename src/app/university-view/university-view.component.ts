import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-university-view',
  templateUrl: './university-view.component.html',
  styleUrls: ['./university-view.component.scss']
})
export class UniversityViewComponent implements OnInit {
  university : any;
  id : string;
  pic : string;

  constructor(private route : ActivatedRoute,
    private database : FirebaseIntegrationService,
    private router: Router) { }

  ngOnInit() {
      this.pic = "";
      this.university = {};
      this.route.params.subscribe(inp =>{
        this.id = inp.id;

        this.database.getUniversity(this.id).subscribe(data =>{
          this.university = data;
        });

        this.database.getDownloadUrl('/Universities/'+this.id+".jpg").subscribe(url=>{
          this.pic =url;
        });
      });

  }

}
