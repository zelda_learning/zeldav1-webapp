import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { ProfileService} from '../profile.service';

@Component({
  selector: 'app-intro-quiz',
  templateUrl: './intro-quiz.component.html',
  styleUrls: ['./intro-quiz.component.scss']
})
export class IntroQuizComponent implements OnInit {
  stage = -1;
  questions : any;
  answerTotals = {'A':0, 'B':0, 'C':0, 'D':0, 'E':0};
  answers = {};
  type = 'pie';

  constructor(private database : FirebaseIntegrationService,
    private profile : ProfileService,
    private router : Router) { }

  ngOnInit() {

    this.database.getIntoQuiz().subscribe(data=>{
      if(data){
        this.questions = data.questions;
      }
      else{
        console.log(data);
      }
    });
  }

  answer(questionNumber, add) {
    this.answers[questionNumber] = add;
    if(this.stage < this.questions.length-1){
        this.setStage(this.stage+1);
    }
    else{
      this.stage++;
      console.log("entering file send/fetch stage");
      for(var i = 0; i<this.questions.length; i++){
        var letter = this.questions[i].label;
        this.answerTotals[letter] += this.answers[i];
      }
      var inputLetters = Object.keys(this.answerTotals);
      var inputs = []
      for(let i of inputLetters){
        inputs.push(this.answerTotals[i]);
      }
      var toWrite = '{"inputs" : ['+ JSON.stringify(inputs)+']}';
      this.saveTextAsFile(toWrite, this.profile.uid+".json");
    }
  }

  getPieData(){
  }

  setStage(stage){
    this.stage = stage;
  }

 saveTextAsFile (data, filename){
   var blob = [
     new Blob([data], {type: 'text/plain'})
   ];
   var file = new File(blob, filename);
   this.database.saveML(file,filename).then(data=>{
       this.router.navigate(["/personality"]);
   });
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}
