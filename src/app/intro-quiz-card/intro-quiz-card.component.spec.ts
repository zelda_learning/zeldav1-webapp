import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntroQuizCardComponent } from './intro-quiz-card.component';

describe('IntroQuizCardComponent', () => {
  let component: IntroQuizCardComponent;
  let fixture: ComponentFixture<IntroQuizCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroQuizCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntroQuizCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
