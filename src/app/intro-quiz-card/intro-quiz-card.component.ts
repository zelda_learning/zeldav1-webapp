import { Component, OnInit } from '@angular/core';
import { ProfileService} from '../profile.service'

@Component({
  selector: 'app-intro-quiz-card',
  templateUrl: './intro-quiz-card.component.html',
  styleUrls: ['./intro-quiz-card.component.scss']
})
export class IntroQuizCardComponent implements OnInit {

  constructor(public profile: ProfileService) { }

  ngOnInit() {
  }

}
