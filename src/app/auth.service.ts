import {Injectable} from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';

@Injectable()
export class AuthService {

    authState: any;

    constructor(public afAuth: AngularFireAuth) {
      this.afAuth.authState.subscribe((data) => {
        this.authState = data;
      });
    }

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((credential) => {
        return credential;
    });
  }

    get isUserAnonymousLoggedIn(): boolean {
      return (this.authState !== null) ? this.authState.isAnonymous : false;
    }

    get currentUserId(): string {
      return (this.authState !== null) ? this.authState.uid : '';
    }

    get currentUserName(): string {
      return this.authState['email'];
    }

    get currentUser(): any {
      return this.authState;
    }

    get isUserEmailLoggedIn(): boolean {
      if ((this.authState !== null) && (!this.isUserAnonymousLoggedIn)) {
        return true;
      } else {
        return false;
      }
    }

    signUpWithEmail(email: string, password: string) {
      return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
        .then((user) => {
          this.authState = user;
          console.log(this.authState.uid + 'created');
          return user;
        })
        .catch(error => {
          console.log(error);
          throw error;
        });
    }

    loginWithEmail(email: string, password: string)  {
      return this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then((user) => {
          this.authState = user.user;
          console.log(this.authState.uid + ' Logged in');
          return user;
        })
        .catch(error => {
          console.log(error);
          throw error;
        });

    }

    signOut(): void {
      this.afAuth.auth.signOut();
    }

    getCurrentUser() {
      const user = this.afAuth.auth.currentUser;
      return user;
    }

    updateEmail(email: string) {
      return this.getCurrentUser().updateEmail(email);
    }

    resetPassword(email: string) {
      return this.afAuth.auth.sendPasswordResetEmail(email);
    }
}
