import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationHorizontalFeedComponent } from './application-horizontal-feed.component';

describe('ApplicationHorizontalFeedComponent', () => {
  let component: ApplicationHorizontalFeedComponent;
  let fixture: ComponentFixture<ApplicationHorizontalFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationHorizontalFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationHorizontalFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
