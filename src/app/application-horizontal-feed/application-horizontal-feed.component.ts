import { Component, OnInit } from '@angular/core';
import { Application } from '../objects/application';
import { FirebaseIntegrationService} from '../firebase-integration.service';

@Component({
  selector: 'app-application-horizontal-feed',
  templateUrl: './application-horizontal-feed.component.html',
  styleUrls: ['./application-horizontal-feed.component.scss']
})
export class ApplicationHorizontalFeedComponent implements OnInit {
  applications : Application[];

  constructor(private database: FirebaseIntegrationService) { }

  ngOnInit() {
    this.database.getApplications().subscribe(data =>{
      this.applications = data;
      });
  }

}
