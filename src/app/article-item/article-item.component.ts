import { Component, OnInit, Input } from '@angular/core';
import { Article } from '../objects/article';
import { FirebaseIntegrationService } from '../firebase-integration.service';

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.scss']
})
export class ArticleItemComponent implements OnInit {
  @Input() item: Article;
  pic : String;
  cont: String;

  constructor(private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.pic = "";
    this.cont = this.item.subtitle.replace(/<\/?[^>]+(>|$)/g, "");
    this.database.getDownloadUrl("/Resources/"+this.item.id+".jpg").subscribe(url => {
      this.pic = url;
    });
  }

}
