import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BursaryTableComponent } from './bursary-table.component';

describe('BursaryTableComponent', () => {
  let component: BursaryTableComponent;
  let fixture: ComponentFixture<BursaryTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BursaryTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BursaryTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
