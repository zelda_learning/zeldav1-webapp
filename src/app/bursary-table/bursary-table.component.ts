import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FavouriteService } from '../favourite.service';
import {FirebaseIntegrationService} from '../firebase-integration.service';
import { map, flatMap, switchMap, filter } from "rxjs/operators";
import {combineLatest, of} from "rxjs";

@Component({
  selector: 'app-bursary-table',
  templateUrl: './bursary-table.component.html',
  styleUrls: ['./bursary-table.component.scss']
})
export class BursaryTableComponent implements OnInit {
  bursaries : Array<any>;
  app : string;

  constructor(private favService : FavouriteService, private database : FirebaseIntegrationService,
    private router : Router) { }

  ngOnInit() {
    this.bursaries= [];
    this.favService.getFavourites().then(fav=>{
      fav.pipe(
        map(favourite=>{
          //console.log(favourite);
          return favourite["pinnedBursaries"];
        }),
        flatMap((pinnedItems:[""])=>{
          //console.log(pinnedItems);
          return pinnedItems;
        }),
        flatMap((id)=>{
          return this.database.getApplication(id+"");
        }),
        flatMap(item=>{
          return combineLatest(
            of(item),
            this.database.getDownloadUrl("Bursaries/"+item['id']+".jpg")
          ).pipe(
            map(([item,pic])=>{
              return {item,pic};
            })
          )
        }),
        filter(item=>{
          var found  = false;
          for(let i of this.bursaries){
            if(i.id == item['item'].id){
              found = true;
              break;
            }
          }
          return !found;
        })
      ).subscribe((data)=>{
        data.item.subtitle = data.item.subtitle.replace(/<\/?[^>]+(>|$)/g, "");
        data.item['image'] = data.pic;
        this.bursaries.push(data.item)
      });
    });
  }

  remove(bursaryId: string){
    var index = -1;
    for(let i of this.bursaries){
      index++;
      if(bursaryId == i.id){
        break;
      }
    }
    this.bursaries.splice(index,1);
    this.favService.remove(bursaryId);
  }

  clickFav(fav){
    this.router.navigate(["/applications/"+fav.id])
  }

}
