import { Component, OnInit } from '@angular/core';
import { Company } from '../objects/company';
import { FirebaseIntegrationService} from '../firebase-integration.service';;
import { Router } from '@angular/router';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  companies : Company[];

  constructor(private database: FirebaseIntegrationService, private router: Router) { }

  ngOnInit() {
      this.database.getCompanies().subscribe(data=>{
        this.companies = data;
      });
  }

}
