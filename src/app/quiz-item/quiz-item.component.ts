import { Component, OnInit, Input} from '@angular/core';
import { FirebaseIntegrationService } from '../firebase-integration.service';
import { catchError, map } from 'rxjs/operators';

@Component({
  selector: 'app-quiz-item',
  templateUrl: './quiz-item.component.html',
  styleUrls: ['./quiz-item.component.scss']
})
export class QuizItemComponent implements OnInit {
  @Input() item :any;
  @Input() percent : number;
  pic : String;

  constructor(private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.pic = "";
    this.database.getDownloadUrl("/Quizzes/"+this.item.id+"/"+this.item.id+".jpg").pipe(
      map(url=>{return url}),
      catchError(err => {return this.database.getDownloadUrl("/Quizzes/"+this.item.id+"/"+this.item.id+".png")}),
      map(url=>{return url})
    ).subscribe(url=>{
      this.pic = url;
    });
  }

}
