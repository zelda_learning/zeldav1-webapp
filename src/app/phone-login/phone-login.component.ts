import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Phone } from '../objects/phone';
import * as firebase from 'firebase/app';
import {WindowService} from '../window.service';
import {AuthService} from '../auth.service';
import {FirebaseIntegrationService} from '../firebase-integration.service';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-phone-login',
  templateUrl: './phone-login.component.html',
  styleUrls: ['./phone-login.component.scss']
})
export class PhoneLoginComponent implements OnInit {

  windowRef: any;
  phoneNumber = new Phone();
  verificationCode: string;
  user: any;
  sent = false;
  result : any;
  error : string;
  signedUp = false;

  constructor(private win: WindowService, private auth : AuthService, private database: FirebaseIntegrationService,
    private router : Router, private afauth : AngularFireAuth) { }

  ngOnInit() {
    this.phoneNumber.country = "+27";
    this.windowRef = this.win.getWindow();
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
    this.windowRef.recaptchaVerifier.render();
  }


  sendLoginCode() {
    const appVerifier = this.windowRef.recaptchaVerifier;
    const num = this.phoneNumber.e164;
    console.log(num)
    this.afauth.auth.signInWithPhoneNumber(num, appVerifier)
            .then(result => {
                this.result = result;
                this.sent = true;
                this.error = null;
            })
            .catch( error => {
              console.log(error);
              this.error = "The phone number is incorrect";
            });
  }

  verifyLoginCode() {
   this.result
                 .confirm(this.verificationCode)
                 .then( result => {
                   // var credential = firebase.auth.PhoneAuthProvider.credential(this.result.verificationId, this.verificationCode);
                   // this.afauth.auth.signInAndRetrieveDataWithCredential(credential);
                   this.user = result.user;
                   console.log(this.user.uid +"Seeking login or signup");
                   this.database.uid = this.user.uid;
                   this.database.getUserState().then(data=>{
                     data.subscribe(data=>{
                         if(data && !this.signedUp){
                           this.database.loggedIn = true;
                           this.database.rClass = "col-md-10 ml-auto margin";
                           this.router.navigate(["/home"]);
                         }
                         else{
                           if(!this.signedUp){
                             this.signedUp = true;
                             this.database.rClass = "col-md-10 ml-auto margin";
                             this.sendSignUpPhone();
                             this.router.navigate(["/signup"]);
                           }
                         }

                     });

                   })
   })
   .catch( error =>{
     console.log(error, "Incorrect code entered?");
     this.error = "incorrect code entered";
   });
 }

  sendSignUpPhone = () => {
    (<any>window).ga('send', 'event', {
      eventCategory: 'SignUp',
      eventLabel: '',
      eventAction: 'SignUpPhone',
      eventValue: 1
    });
  }

}
