import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FirebaseIntegrationService} from'../firebase-integration.service';
import { map, switchMap} from "rxjs/operators";
import {Observable} from "rxjs/observable";
import {ApplicationService} from "../application.service";
import {ProfileService} from "../profile.service";

@Component({
  selector: 'app-application-submit',
  templateUrl: './application-submit.component.html',
  styleUrls: ['./application-submit.component.scss']
})
export class ApplicationSubmitComponent implements OnInit {
  route : string;
  id : string;
  uid : string;
  formId : string;
  @Input() userFormId : string;
  application : any;
  sections : number ;
  navItems : number[] = [];
  letters : string;

  constructor(private profile : ProfileService, private router: ActivatedRoute, private database: FirebaseIntegrationService, private router2 : Router, private appService : ApplicationService) { }

  ngOnInit() {
    this.letters = "ABCDEFGH";
    this.userFormId = this.appService.userFormId;
    this.uid = this.profile.uid;
    this.router.params.pipe(
      switchMap(url =>{
        this.id = url.id
        if(!this.profile.getUserVerified() || !this.profile.userState.progress || this.profile.userState.progress ==0){
          this.router2.navigate(["/applications/"+this.id+"/apply/signup"])
        }
        else{
          return this.database.getApplication(url.id);
        }
      })
    ).subscribe (app=>{
      this.sections = app['sections'];
      for(var i =0; i<this.sections; i++){
        this.navItems.push(i);
      }
      console.log(this.navItems);
    });
  }

  submit(){
    this.database.submitUserApplicationForm(this.userFormId);
    this.database.addSumittedFormToState(this.uid, this.userFormId,this.id).then(data=>{
      data.subscribe(data=>{
        console.log(data);
        if (this.appService.unAuth) {
          this.router2.navigate(["/thankyou"]);
        } else {
          this.router2.navigate(["/home"]);
        }
      });
    });
  }

}
