import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileSchoolInfoComponent } from './profile-school-info.component';

describe('ProfileSchoolInfoComponent', () => {
  let component: ProfileSchoolInfoComponent;
  let fixture: ComponentFixture<ProfileSchoolInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileSchoolInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileSchoolInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
