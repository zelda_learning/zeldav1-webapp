import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import {FirebaseIntegrationService} from '../firebase-integration.service';
@Component({
  selector: 'app-profile-school-info',
  templateUrl: './profile-school-info.component.html',
  styleUrls: ['./profile-school-info.component.scss']
})
export class ProfileSchoolInfoComponent implements OnInit {
  matricUploaded = false;
  reportUploaded = false;
  reportUrl: string;
  private matricUrl: string;

  constructor(public  profile: ProfileService, private database: FirebaseIntegrationService) {
   }

  ngOnInit() {
    if (this.profile.userSchool.schoolReportUploaded && this.profile.userSchool.schoolReportUploaded !== 'N/A') {
      this.reportUploaded = true;
      const ext = this.profile.userSchool.schoolReportUploaded.substr
      (this.profile.userSchool.schoolReportUploaded.indexOf('.'), this.profile.userSchool.schoolReportUploaded.length);
      if (ext === '.pdf') {
        this.database.getDownloadUrl('/UserProfile_pdf/schoolReport/' + this.profile.userSchool.schoolReportUploaded).subscribe(data2 => {
          this.reportUrl = data2 ;
        });
      } else {
        this.database.getDownloadUrl('UserProfile_image/schoolReport/' + this.profile.userSchool.schoolReportUploaded).subscribe(data2 => {
          this.reportUrl = data2 ;
        });
      }
    }
    if (this.profile.userSchool.matricCertificateUploaded && this.profile.userSchool.matricCertificateUploaded !== 'N/A') {
      this.matricUploaded = true;
      const ext = this.profile.userSchool.matricCertificateUploaded.substr
      (this.profile.userSchool.matricCertificateUploaded.indexOf('.'), this.profile.userSchool.matricCertificateUploaded.length);
      if (ext === '.pdf') {
        this.database.getDownloadUrl
        ('/UserProfile_pdf/matricCertificate/' + this.profile.userSchool.matricCertificateUploaded).subscribe(data2 => {
          this.matricUrl = data2 ;
        });
      } else {
        this.database.getDownloadUrl
        ('UserProfile_image/matricCertificate/' + this.profile.userSchool.matricCertificateUploaded).subscribe(data2 => {
          this.matricUrl = data2 ;
        });
      }
    }
  }

  addSubject() {
    this.profile.addSubject();
  }

  removeSubject() {
    this.profile.removeSubject();
  }

  trackByFn(index, item) {
    return item.id;
  }

  upReport(event) {
    console.log( event.target.files[0]);

    let imageName = '';
    let fileName = '';
    if (event.target.files[0].type === 'application/pdf') {
      imageName = '/UserProfile_pdf/schoolReport/';
      fileName = this.profile.uid + '.pdf';
    } else {
      imageName = '/UserProfile_image/schoolReport/' ;
      fileName = this.profile.uid + '.jpg';
    }

    this.profile.saveRes(event.target.files[0], imageName + fileName).then(data => {
      this.profile.userSchool.schoolReportUploaded = fileName;
      this.reportUploaded = true;
      const ext = this.profile.userSchool.schoolReportUploaded.substr
      (this.profile.userSchool.schoolReportUploaded.indexOf('.'), this.profile.userSchool.schoolReportUploaded.length);
      if (ext === '.pdf') {
        this.database.getDownloadUrl('/UserProfile_pdf/schoolReport/' + this.profile.userSchool.schoolReportUploaded).subscribe(data2 => {
          this.reportUrl = data2 ;
        });
      } else {
        this.database.getDownloadUrl('UserProfile_image/schoolReport/' + this.profile.userSchool.schoolReportUploaded).subscribe(data2 => {
          this.reportUrl = data2 ;
        });
      }
    });
  }

  upMatric(event) {
    console.log( event.target.files[0]);
    let imageName = '';
    let fileName = '';
    if (event.target.files[0].type === 'application/pdf') {
      imageName = '/UserProfile_pdf/matricCertificate/';
      fileName = this.profile.uid + '.pdf';
    } else {
      imageName = '/UserProfile_image/matricCertificate/' ;
      fileName = this.profile.uid + '.jpg';
    }

    this.profile.saveRes(event.target.files[0], imageName + fileName).then(data => {
      this.profile.userSchool.matricCertificateUploaded = fileName;
      this.matricUploaded = true;
      const ext = this.profile.userSchool.matricCertificateUploaded.substr
      (this.profile.userSchool.matricCertificateUploaded.indexOf('.'), this.profile.userSchool.matricCertificateUploaded.length);
      if (ext === '.pdf') {
        this.database.getDownloadUrl
        ('/UserProfile_pdf/matricCertificate/' + this.profile.userSchool.matricCertificateUploaded).subscribe(data2 => {
          this.reportUrl = data2 ;
        });
      } else {
        this.database.getDownloadUrl
        ('UserProfile_image/matricCertificate/' + this.profile.userSchool.matricCertificateUploaded).subscribe(data2 => {
          this.reportUrl = data2 ;
        });
      }
    });
  }

  removeReport() {
    this.reportUploaded = false;
    delete this.profile.userSchool.schoolReportUploaded;
  }

  goToLink(url: string) {
    window.open(url, '_blank');
  }

  removeMatric() {
    this.matricUploaded = false;
    delete  this.profile.userSchool.matricCertificateUploaded;
  }

  getSchoolClass(attr: string) {
    return {
      'is-valid' : this.profile.userSchool[attr] ,
      'is-invalid' : !this.profile.userSchool[attr]
    };
  }

  getProfileClass(attr: string) {
    return {
      'is-valid' : this.profile[attr] ,
      'is-invalid' : !this.profile[attr]
    };
  }
}
