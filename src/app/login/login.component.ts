import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseIntegrationService} from '../firebase-integration.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  password: string;
  user: string;
  error : string;
  signedUp : boolean;
  method : string = "";

  constructor(private database: FirebaseIntegrationService, private router: Router) { }

  ngOnInit() {
    this.signedUp = false;
    if(this.database.loggedIn){
      this.router.navigate(["/home"]);
    }
  }

  logInGoogle(){
    this.database.logInGoogle().then(data=>{
      console.log(data.user.uid +"Seeking login or signup");
      this.database.uid = data.user.uid;
      this.database.getUserState().then(data=>{
        data.subscribe(data=>{
          if(data && !this.signedUp){
            this.database.loggedIn = true;
            this.database.rClass = "col-md-10 ml-auto margin";
            this.router.navigate(["/home"]);
          }
          else{
            if(!this.signedUp){
              this.signedUp = true;
              this.database.rClass = "col-md-10 ml-auto margin";
              this.sendSignUpGoogle();
              this.router.navigate(["/signup"]);
            }
          }
        });
      });
    })
    .catch(error =>{
      console.log(error);
    });
  }

  sendSignUpGoogle = () => {
    (<any>window).ga('send', 'event', {
      eventCategory: 'SignUp',
      eventLabel: 'SignUpGoogle',
      eventAction: '',
      eventValue: 1
    });
  }

  changeMethod(method) {
    this.method = method;
  }


}
