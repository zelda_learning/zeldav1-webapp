import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseIntegrationService} from'../firebase-integration.service';
import { map, switchMap} from "rxjs/operators";
import { ApplicationService } from "../application.service";
import { ProfileService} from "../profile.service";

@Component({
  selector: 'app-application-submit-intro',
  templateUrl: './application-submit-intro.component.html',
  styleUrls: ['./application-submit-intro.component.scss']
})
export class ApplicationSubmitIntroComponent implements OnInit {
  app: any = {};
  id : any;
  constructor(public profile: ProfileService, private router: ActivatedRoute, private database: FirebaseIntegrationService, private appService : ApplicationService) { }

  ngOnInit() {
    this.appService.intro = true;
    if(this.profile.userState.progress && this.profile.userState.progress !=0){
      this.router.params.pipe(
        switchMap(url=>{
          this.id = url.id;
          return this.database.getApplication(url.id);
        })
      ).subscribe(app=>{
        this.app = app;
      });
    }
    else{
      this.router.params.subscribe(url=>{
        this.id = url.id;
      })
      this.app = {title: "GRRRL Camp",
                  sections : 1,
                  applicationProcess : "Click on the APPLY button to start your application!"}
    }
  }

}
