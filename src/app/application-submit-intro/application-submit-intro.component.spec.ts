import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationSubmitIntroComponent } from './application-submit-intro.component';

describe('ApplicationSubmitIntroComponent', () => {
  let component: ApplicationSubmitIntroComponent;
  let fixture: ComponentFixture<ApplicationSubmitIntroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationSubmitIntroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationSubmitIntroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
