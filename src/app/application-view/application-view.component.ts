import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location} from '@angular/common';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { Router } from '@angular/router';
import { FavouriteService } from '../favourite.service';

@Component({
  selector: 'app-application-view',
  templateUrl: './application-view.component.html',
  styleUrls: ['./application-view.component.scss']
})
export class ApplicationViewComponent implements OnInit {
  application : any;
  id : string;
  pic : string;
  favourite = false;

  constructor(private route : ActivatedRoute,
    private database : FirebaseIntegrationService,
    private router : Router,
    private favService : FavouriteService,
    private location: Location) {}

  ngOnInit() {
      this.pic = "";
      this.application = "";
      this.route.params.subscribe(inp =>{
        this.id = inp.id;
        this.database.getApplication(this.id).subscribe(data =>{
          this.application = data;
        });
        this.database.getDownloadUrl("/Bursaries/" + this.id +".jpg").subscribe(data=>{
          this.pic = data;
        });
      });
      this.favourite = this.favService.isFavourite(this.application.id);
      window.scrollTo(0,0);
  }

  favouriteEvent(e){
    this.favourite = !this.favourite;
    if(this.favourite){
      this.favService.add(this.application.id)
    }
    else{
      this.favService.remove(this.application.id);
    }
  }

  back() {
    this.location.back();
  }
}
