import { Component, OnInit, Input } from '@angular/core';
import { FirebaseIntegrationService } from '../firebase-integration.service';
import { FavouriteService } from '../favourite.service';
import { map, catchError} from 'rxjs/operators';

@Component({
  selector: 'app-application-item',
  templateUrl: './application-item.component.html',
  styleUrls: ['./application-item.component.scss']
})
export class ApplicationItemComponent implements OnInit {
  @Input() item : any;
  favourite : boolean = false;
  pic: String;
  app : String;

  constructor(private database: FirebaseIntegrationService, private favService : FavouriteService) { }

  ngOnInit() {
    this.pic = '';
    if (this.item.subtitle) {
        this.app = this.item.subtitle.replace(/<\/?[^>]+(>|$)/g, '');
    } else {
      this.app = '';
    }
    this.database.getDownloadUrl('/Bursaries/' + this.item.id + '.jpg').pipe(
      map(url => url ),
      catchError(err => {
        return this.database.getDownloadUrl('/Bursaries/' + this.item.id + '.png');
      }),
      map(url => url)
    ).subscribe(url => {this.pic = url + ''; } );
    this.favourite = this.favService.isFavourite(this.item.id);
  }

  favouriteEvent(e) {
    this.favourite = !this.favourite;
    if (this.favourite) {
      this.favService.add(this.item.id)
    } else {
      this.favService.remove(this.item.id);
    }
  }

}
