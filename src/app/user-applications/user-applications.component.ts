import { Component, OnInit } from '@angular/core';
import { FirebaseIntegrationService } from '../firebase-integration.service';
import { switchMap, map } from 'rxjs/operators';

@Component({
  selector: 'app-user-applications',
  templateUrl: './user-applications.component.html',
  styleUrls: ['./user-applications.component.scss']
})
export class UserApplicationsComponent implements OnInit {
  startedApps : any;
  completedApps : any;
  startedAppsKeys = [];
  completedAppsKeys = [];
  constructor(private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.database.getUserState().then(data=>{
      data.pipe(
        map(state=>{
          console.log(state);
          if(state['startedApplicationIds']){
            this.startedApps = state['startedApplicationIds'];
          }
          if(state['completedApplicationIds']){
            this.completedApps = state['completedApplicationIds'];
          }
          return 0;
        })
      ).subscribe(data=>{
        if(this.startedApps){
          this.startedAppsKeys = Object.keys(this.startedApps);
        }
        if(this.completedApps){
          this.completedAppsKeys = Object.keys(this.completedApps);
        }
      });
    })
  }

}
