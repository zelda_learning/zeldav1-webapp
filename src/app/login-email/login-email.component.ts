import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import {ProfileService} from '../profile.service';

@Component({
  selector: 'app-login-email',
  templateUrl: './login-email.component.html',
  styleUrls: ['./login-email.component.scss']
})
export class LoginEmailComponent implements OnInit {
  password: string;
  user: string;
  error : string;
  signedUp : boolean;

  constructor(private database: FirebaseIntegrationService, private router: Router, private profile: ProfileService) { }

  ngOnInit() {
  }

  logIn(){
    this.database.logIn(this.user.trim(),this.password).then(
      (data)=>{
        this.database.loggedIn = true;
        this.database.rClass = "col-md-10 ml-auto margin";
        this.router.navigate(["/home"]);
    })
    .catch(error=>{
      this.error = "Your username or password is incorrect";
      if(this.password == "")
        this.error = "Please enter your password";
      if(this.user ==  "")
        this.error = "Please enter a user email address";
    });
  }

  signUp(){
    this.database.signUp(this.user,this.password).then(su=> {
        this.database.logIn(this.user,this.password).then((data)=>{
          this.database.loggedIn = true;
          console.log("su is " +this.signedUp);
          if(!this.signedUp){
            this.signedUp = true;
            console.log("navigating");
            this.sendSignUpEmail();
            this.router.navigate(["/signup"]);
          }
        });
    })
    .catch((error)=>{
        this.error = "A user already exists with this email address or the email address is incorrect";
        if(this.password == "")
          this.error = "Please enter your password";
        if(this.user ==  "")
          this.error = "Please enter a user email address";
    });
  }

  forgotPassword() {
    this.profile.forgotPassword(this.user).then(ret => {
        this.error = 'Email successfully sent';
    }).catch( err => {
      if (!this.user || this.user === '') {
        this.error = 'Please enter an email address in the box above';
      } else {
        this.error = 'The email address does not exist';
      }
    });
  }

  sendSignUpEmail = () => {
    (<any>window).ga('send', 'event', {
      eventCategory: 'SignUp',
      eventLabel: '',
      eventAction: 'SignUpEmail',
      eventValue: 1
    });
  }
}
