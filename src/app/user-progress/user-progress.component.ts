import { Component, OnInit } from '@angular/core';
import { UserProfile } from '../objects/userProfile';
import { FirebaseIntegrationService } from '../firebase-integration.service';
import { ProfileService } from '../profile.service';
import {switchMap} from 'rxjs-compat/operator/switchMap';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-user-progress',
  templateUrl: './user-progress.component.html',
  styleUrls: ['./user-progress.component.scss']
})
export class UserProgressComponent implements OnInit {
  up : any;
  userState: any;
  pic : String;
  private options: any;
  private data: any;

  constructor(private database : FirebaseIntegrationService, public profile : ProfileService) { }

  ngOnInit() {
    this.database.getUserState().then(us => {
      us.subscribe( userstate => {
        this.data = {
          labels: ['Profile finished', 'Profile not finished'],
          datasets: [
            {
              label: 'progress',
              data: [userstate.progress, 100 - userstate.progress],
              backgroundColor : [ '#168582', '#DDDDDD']
            }
          ]
        };
      })
    })
    this.options = {
      cutoutPercentage : 50,
      responsive: true,
      maintainAspectRatio: true,
      colors : [ '#ff8c9d'],
      legend : false
    };

  }

  submit() {
    this.profile.userState.submitted = true;
    this.profile.update();
    this.database.submitCBAForm().then( cba => {
      cba.subscribe(data => console.log(data));
      (<any>window).ga('send', 'event', {
        eventCategory: 'UserProgress',
        eventLabel: '',
        eventAction: 'CBASubmitted',
        eventValue: 1
      });
      cba.unsubscribe();
    });
  }
}
