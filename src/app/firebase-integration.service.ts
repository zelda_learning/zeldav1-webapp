import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AuthService} from './/auth.service';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { map, catchError, switchMap } from 'rxjs/operators';
import * as firebase from 'firebase/app';
import {environment} from 'environments/environment';


@Injectable()
export class FirebaseIntegrationService {
  user : any;
  userdata: any
  loggedIn: boolean = false;
  su: any;
  uid : String;
  rClass = "ml-auto";
  storageb : any;
  noMenu = false;

  constructor(
          private afStorage : AngularFireStorage,
          private auth : AuthService,
          private router: Router,
          private afs : AngularFirestore
        ) {
            afs.firestore.settings({timestampsInSnapshots: true});
            // afs.firestore.enablePersistence().then(data=>{
            //   console.log("persistence enabled");
            // });
            // firebase.firestore.setLogLevel("debug");
            afs.firestore.enableNetwork().then(data=>{
              console.log("network enebled");
            });
            //console.log(environment.bucket);
            this.storageb = firebase.app().storage(environment.bucket).ref();
            //console.log(this.storageb);
            this.user = new Promise((resolve, reject) => {
              if(this.uid && this.userdata){
                resolve(this.userdata);
              }
              else{
                this.auth.afAuth.authState.subscribe((data)=>{
                  if(data){
                    if(data.uid){
                      this.userdata = data;
                      this.uid = data.uid;
                      this.loggedIn = true;
                      resolve(data);
                    }
                    else{
                      console.log(data);
                    }
                  }
                });
              }
            });
        }


    getResources(){
      var res: Observable<Array<any>>;
      res = this.afs.collection('Tips').valueChanges();
      return res;
    }

    getResource(id : String){
      var res: Observable<any>;
      res = this.afs.doc<any>('Tips/'+id).valueChanges();
      return res;
    }

    getUniversities(){
      var res: Observable<Array<any>>;
      res = this.afs.collection('Universities').valueChanges();
      return res;
    }


    getUniversity(id : String ){
      var res: Observable<any>;
      res = this.afs.doc<any>('Universities/'+id).valueChanges();
      return res;
    }

    getCompanies(){
      var res: Observable<Array<any>>;
      res = this.afs.collection('Companies').valueChanges();
      return res;
    }

    getCompany(id: String){
      var res: Observable<any>;
      res = this.afs.doc<any>('Companies/'+id).valueChanges();
      return res;
    }

    getApplications(){
      var res: Observable<Array<any>>;
      res = this.afs.collection('Bursaries',
          ref => ref
            .where('closingDate', '>' , new Date())
            .orderBy('closingDate', 'asc')).valueChanges();
      return res;
    }

    getApplication(id : String){
      var res: Observable<any>;
      res = this.afs.doc<any>('Bursaries/'+id).valueChanges();
      return res;
    }

    getDownloadUrl(reference : string){
      if(reference && reference.indexOf("undefined")==-1){
        var pathReference = this.afStorage.ref(reference);
        return pathReference.getDownloadURL();
      }
      else{
        return of("");
      }
    }

    getUID(){
      return this.user.then(user =>{
        if(this.uid){
          return this.uid;
        }
        else{
          return of("");
        }
      });
    }

    getUserPersonal(){
      var res: Observable<any>;
      return this.user.then(user =>{
        res = this.afs.doc<any>('UserProfilePersonal/' +this.uid).valueChanges();
        return res;
      });
    }

    getUserSchool(){
      var res: Observable<any>;
      return this.user.then(user =>{
        res = this.afs.doc<any>('UserProfileSchool/' +this.uid).valueChanges();
        return res;
      });
    }

    getUserState(){
      var res: Observable<any>;
      return this.user.then(uid =>{
        res = this.afs.doc<any>('UserState/' +this.uid).valueChanges();
        return res;
      }).catch(error=>{
        console.log(error);
      });
    }

    getUserPersonality(){
      var res: Observable<any>;
      return this.user.then(uid =>{
        res = this.afs.doc<any>('UserProfilePersonality/' +this.uid).valueChanges();
        return res;
      }).catch(error=>{
        console.log(error);
      });
    }

    getUserMessages(){
      var res: Observable<Array<any>>;
      res = this.afs.collection<any>('Messages/' +this.uid +'/Messages', ref=>ref.orderBy('messageSendTime')).valueChanges();
      return res;
    }

    getNewMessages() {
      return this.afs.doc('Messages/' + this.uid).valueChanges();
    }

    messagesRead() {
      return this.afs.doc('Messages/' + this.uid).update({newMessageReceived : false});
    }

    sendUserMessage(message:any){
      return this.afs.collection<any>('Messages/' +this.uid +'/Messages').add(message);
    }

    updateUser(user : any){
      this.getUID().then(uid=>{
        this.afs.doc<any>('UserProfile/' + uid).update(user);
      })
    }

    updateUserState(userState: any){
      this.getUID().then(uid=>{
        this.afs.doc<any>('UserState/'+uid).update(userState);
      });
    }

    updateUserSchool(userEducation : any){
      this.getUID().then(uid=>{
        this.afs.doc<any>('UserProfileSchool/'+uid).update(userEducation);
      });
    }

    updateUserPersonal(userAd: any){
      this.getUID().then(uid=>{
        this.afs.doc<any>('UserProfilePersonal/'+uid).update(userAd);
      });
    }

    updateFav(pinnedBursariesIn : Array<any>){
      this.getUID().then(uid=>{
        this.afs.doc<any>('UserState/'+uid).update({pinnedBursaries : pinnedBursariesIn});
      });
    }

    logOut(){
      this.loggedIn = false;
      this.auth.signOut();
    }

    logIn(user: string, password: string){
      this.user = this.auth.loginWithEmail(user,password).then(data=>{
        if(data){
          this.loggedIn = true;
          this.rClass = "col-md-10 ml-auto margin";
        }
      });
      return this.user;
    }

    logInNoMenu(user: string, password: string) {
      this.user = this.auth.loginWithEmail(user, password).then(data => {
        if (data) {
          this.noMenu = true;
          this.rClass = "ml-auto";
          this.loggedIn = true;
        }
      });
      return this.user;
    }

    signUp(email:string, password:string){
      this.user = new Promise((resolve, reject) => {
        if(this.uid && this.userdata){
          resolve(this.userdata);
        }
        this.auth.afAuth.authState.subscribe((data)=>{
          if(data){
            if(data.uid){
              this.userdata = data;
              this.uid = data.uid;
              resolve(data);
            }
            else{
              console.log(data);
            }
          }
        });
      });
       this.su = this.auth.signUpWithEmail(email, password);
       this.loggedIn = false;
       return this.su;
    }

    createUserPersonal(uid: string){
      return this.afs.doc<any>("UserProfilePersonal/"+uid).set({
          id : uid,
      });
    }

    createUserState(uid: string){
      return this.afs.doc<any>('UserState/'+uid).set({
          fieldPreference : {},
          completedNBTQuizzes : {},
          pinnedBursaries : [],
          progress : 15,
          quote : "Welcome to Zelda",
          title : "",
          username : "",
          startedApplications : {},
          optIn : false
        });
    }

    createUserSchool(uid : string){
      return this.afs.doc<any>('UserProfileSchool/'+uid).set({
          id : uid,
          subjectMarksMostRecent : {}
        });
    }

    getQuizzes(){
      var res: Observable<Array<any>>;
      res = this.afs.collection('NBTQuizzes').snapshotChanges().pipe(map(changes=>{
        return changes.map(action=>{
          var data = action.payload.doc.data();
          data['id'] = action.payload.doc.id;
          return data;
        });
      }));
      return res;
    }

    getQuiz(id: string){
      var res: Observable<any>;
      res = this.afs.doc<any>('NBTQuizzes/'+id).valueChanges();
      return res;
    }

    getApplicationForm(id : string, letter: string ){
      var res: Observable<any>;
      res = this.afs.doc<any>('ApplicationForm'+letter +"/"+id).valueChanges();
      return res;
    }

    getUnauthApplicationForm(){
      var res: Observable<any>;
      res = this.afs.doc<any>('UnauthObjects/GRRL').valueChanges();
      return res;
    }

    getUserApplicationForm(docId: string){
      var res: Observable<any>;
      console.log(docId);
      res = this.afs.doc('ApplicationForm/'+docId).valueChanges();
      return res;
    }

    getUserFormFragment(id : string, letter: string ){
      var res: Observable<any>;
      console.log('UserApplicationForm'+letter +"/"+id);
      res = this.afs.doc<any>('UserApplicationForm'+letter +"/"+id).valueChanges();
      return res;
    }

    createApplicationForm(uid :string, formId: string){
      return this.afs.collection('ApplicationForm/').add({
          applicationId : formId,
          submitted : false,
          userId : uid
      });
    }

    createCBAForm() {
    return this.afs.collection('ApplicationForm/').add({
      applicationId : 'CBA',
      submitted : false,
      userId : this.uid
    }) ;
    }

    addCbaToState(formID: string) {
      return this.afs.doc('UserState/'+this.uid).update({
        optIn : formID
      });
    }

    submitCBAForm(){
      return this.getUserState().then(user=>{
        return user.pipe(
          map((us:any) => {
            return us.optIn ;
          }),
          switchMap( (formId:any) =>{
            return this.afs.doc('ApplicationForm/' + formId).update({
              submitted : true
            });
          })
        );
      })
    }

    addSumittedFormToState(uid:string, formId: string, appId: string){
      var userState;
      return this.getUserState().then(user=>{
        return user.pipe(
          map(us=>{
            userState = us;
            return us['startedApplicationIds'];
          }),
          catchError(err=>{
            console.log(err);
            return null;
          }),
          switchMap((apps:any)=>{
            delete apps[appId];
            console.log(uid);
            this.afs.doc('UserState/' + uid).update({
              startedApplicationIds : apps
            });
            return of("")
          }),
          map(doc=>{
            console.log(doc);
            return userState['completedApplicationIds'];
          }),
          catchError(err=>{
            return null;
          }),
          switchMap((apps:any)=>{
            if(!apps){
              apps = {}
            }
            apps[appId] = formId;
            console.log(apps);
            return this.afs.doc('UserState/' + uid).update({
              completedApplicationIds : apps
            });
          })
        )
      })
    }

    addFormToState(uid:string, formId: string, appId: string){
      console.log(uid);
      return this.getUserState().then(user=>{
        return user.pipe(
          map(us=>{
            console.log(us);
            return us['startedApplicationIds'];
          }),
          catchError(err=>{
            console.log(err);
            return null;
          }),
          switchMap((apps:any)=>{
            if(apps){
              apps[appId] = formId;
            }
            else{
              console.log("apps empty");
              apps = {};
              apps[appId] = formId;
            }
            console.log(apps);
            return this.afs.doc('UserState/' + uid).update({
              startedApplicationIds : apps
            });
          })
        )
      })

    }

    updateApplicationForm(section:string, id: string, fields:any){
      return this.afs.doc<any>('UserApplicationForm'+section+'/'+id).update(fields);
    }

    createUserApplicationForm(section:string, id: string, fields:any){
      console.log(fields);
      return this.afs.doc<any>('UserApplicationForm'+section+'/'+id).set(fields);
    }

    submitUserApplicationForm(id){
      return this.afs.doc<any>('ApplicationForm/'+id).update({submitted:true});
    }

    saveRes(res : any, path: any){
      return this.afStorage.upload(path, res);
    }

    saveML(res :any, path:any){
      var ref = this.storageb.child(path);
      return ref.put(res).then(data=>{
        return data;
      });
    }

    logInGoogle(){
      this.user = this.auth.googleLogin();
      return this.user;
    }

    getIntoQuiz(){
      var res: Observable<any>;
      res = this.afs.doc<any>('Quizzes/INTRO_QUIZ').valueChanges();
      return res;
    }

    saveToken(token){
      console.log("saving",token,this.uid);
      return this.afs.doc<any>('UserRegistrationTokens/'+this.uid).set({'token' : token});
    }

}
