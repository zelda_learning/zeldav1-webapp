import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { FirebaseIntegrationService } from './firebase-integration.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor(private afMessaging: AngularFireMessaging, private database : FirebaseIntegrationService) { }

  startNotifications(){
    this.afMessaging.requestToken
        .subscribe(
          (token) => {
            console.log('Permission granted! Save to the server!', token);
            this.database.saveToken(token);
         },
          (error) => { console.error(error); },
        );
    this.afMessaging.messages
         .subscribe((message) => { console.log(message); });
  }
}
