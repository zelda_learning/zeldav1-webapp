import { Component, OnInit } from '@angular/core';
import { FirebaseIntegrationService} from '../firebase-integration.service';;
import { Router } from '@angular/router';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {
  quizzes : any[];
  userState : any;

  constructor(private database : FirebaseIntegrationService, private router: Router) { }

  ngOnInit() {
    this.userState = {completedNBTQuizzes:{}};
    this.database.getUserState().then(data=>{
      data.subscribe(data=>{
        this.userState = data;
      });
    });
    this.database.getQuizzes().subscribe(data=>{
      this.quizzes = data;
    });
  }

  getPercent(quiz :any){
    var percent = 0;
    if(this.userState.completedNBTQuizzes[quiz.id]){
      percent = this.userState.completedNBTQuizzes[quiz.id] / quiz.answers.length * 100;
    }
    return percent;
  }

}
