import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { QuestionComponent} from '../question/question.component';
import { Router } from '@angular/router';
import { QuizAnswerService} from '../quiz-answer.service';
import {map, switchMap, catchError} from 'rxjs/operators';

@Component({
  selector: 'app-quiz-view',
  templateUrl: './quiz-view.component.html',
  styleUrls: ['./quiz-view.component.scss']
})
export class QuizViewComponent implements OnInit {
  userState : any;
  quiz : any;
  id : string;
  pic : string;
  answers : any;
  length : number;
  correct : number;
  @ViewChildren(QuestionComponent) questionComps :  QueryList<QuestionComponent>;

  constructor(private route : ActivatedRoute,
      private database : FirebaseIntegrationService,
      private router: Router,
      private ansService : QuizAnswerService) { }

  ngOnInit() {
      this.quiz = {};
      this.answers = {};
      this.correct = 0;
      this.pic = "";
      this.database.getUserState().then(data=>{
        data.subscribe(data=>{
          this.userState = data;
        });
      });
      this.route.params.pipe(
        switchMap(url=>{
          this.id = url.id
          return this.database.getQuiz(this.id);
        }),
        map(quiz=>{
          console.log(quiz);
          this.quiz = quiz;
          this.length = this.quiz.answers.length;
          return this.database.getDownloadUrl("/Quizzes/" + this.id +"/" +this.id +".jpg")
        }),
        catchError(err=>{
          return this.database.getDownloadUrl("/Quizzes/" + this.id +"/"+this.id+".png")
        }),
        switchMap(url=>{
          return url;
        })
      ).subscribe((url:string)=>{
        this.pic = url;
      });/*
      this.route.params.subscribe(inp =>{
        this.id = inp.id;
        this.database.getQuiz(this.id).subscribe(data=>{
          this.quiz = data;
          this.length = this.quiz.answers.length;
          this.database.getDownloadUrl("/Quizzes/" + this.id +"/" +this.id +".jpg").subscribe(
            data=>
            {
              this.pic = data;
            },

            err=>
            {
              this.database.getDownloadUrl("/Quizzes/" + this.id +"/"+this.id+".png").subscribe(
                data => {
                  this.pic = data;
                });
            }
          );
        });
      });*/

  }

  checkAnswers(){
    this.questionComps.forEach(
      data=>{
        data.checkAnswer();
      }
    );
  }

  getAnswers(){
    this.questionComps.forEach(
      data=>{
        this.answers[data.questionNum] = data.getAnswer();
        if(data.isCorrect()){
          this.correct++;
        }
      }
    );
    this.userState.completedNBTQuizzes[this.id] = this.correct;
    this.ansService.answers = this.answers;
    this.ansService.id = this.quiz.id;
    this.database.updateUserState(this.userState);

    this.router.navigate(['/checkAnswers/'+this.quiz.id])
  }

}
