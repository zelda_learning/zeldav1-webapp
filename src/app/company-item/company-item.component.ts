import { Component, OnInit, Input } from '@angular/core';
import { Company } from '../objects/company';
import { FirebaseIntegrationService } from '../firebase-integration.service';

@Component({
  selector: 'app-company-item',
  templateUrl: './company-item.component.html',
  styleUrls: ['./company-item.component.scss']
})
export class CompanyItemComponent implements OnInit {
  @Input() item:Company
  pic: String

  constructor(private database: FirebaseIntegrationService) { }

  ngOnInit() {
    this.pic = "";
    this.database.getDownloadUrl("/Companies/"+this.item.id+".jpg").subscribe(url =>{
      this.pic = url;
    });
  }

}
