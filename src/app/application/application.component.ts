import { Component, OnInit } from '@angular/core';
import { Application } from '../objects/application';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { ProfileService } from '../profile.service';
import { Router } from '@angular/router';
import {FIELDS} from '../objects/fields';


@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {
  fields = FIELDS;
  applications : Application[];
  pic : String[];
  filter : String = "All";
  userState : any;
  searchText: String = '';

  constructor(private database: FirebaseIntegrationService, private router:Router,
  private profile : ProfileService) { }

  ngOnInit() {
      this.pic = [];
      this.userState = {fieldPreference : {}};
      this.database.getApplications().subscribe(data =>{
        this.applications = data;
      });

      this.userState = this.profile.userState;
      console.log(this.userState.fieldPreference);
  }

  contains(app : any){
    var found : boolean = false;
    //console.log(app);
    if(!app.applicableFields){
      app.applicableFields = app.field;
    }
    for(var i=0; i<app.applicableFields.length && !found; i++){
      if(this.userState.fieldPreference[app.applicableFields[i]]){
        found = true;
      }
    }
    return found ;
  }

}
