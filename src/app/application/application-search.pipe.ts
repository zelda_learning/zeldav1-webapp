import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'searchApplication'
})

export class ApplicationSearchPipe implements PipeTransform {
  transform(applications: any[], searchText: string): any[] {
    if (applications) {
      applications = applications.filter(application => {
        if (searchText === '') {
          return true;
        } else if (typeof application === 'undefined') {
          return false;
        } else if (application.title.toLowerCase().includes(searchText.toLowerCase())) {
          return true;
        }
        return false;
      });
      return applications;
    }
  }
}
