import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserApplicationCardComponent } from './user-application-card.component';

describe('UserApplicationCardComponent', () => {
  let component: UserApplicationCardComponent;
  let fixture: ComponentFixture<UserApplicationCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserApplicationCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserApplicationCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
