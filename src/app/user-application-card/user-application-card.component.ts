import { Component, OnInit, Input } from '@angular/core';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { FavouriteService } from '../favourite.service';
import { map , catchError} from "rxjs/operators";

@Component({
  selector: 'app-user-application-card',
  templateUrl: './user-application-card.component.html',
  styleUrls: ['./user-application-card.component.scss']
})
export class UserApplicationCardComponent implements OnInit {
  @Input() app : any;
  @Input() submitted : boolean;
  bursary : any;
  pic : string;
  favourite : boolean;
  progclass = "rev";

  constructor(private database : FirebaseIntegrationService, private favService : FavouriteService) { }

  ngOnInit() {
    this.database.getApplication(this.app).subscribe(data=>{
      data.subtitle = data.subtitle.replace(/<\/?[^>]+(>|$)/g, "");
      this.bursary = data;
      this.database.getDownloadUrl("/Bursaries/"+this.bursary.id+".jpg").pipe(
        map(url=>{return url}),
        catchError(err =>{
          return this.database.getDownloadUrl("/Bursaries/"+this.bursary.id+".png")
        }),
        map(url => {return ""+url})
      ).subscribe(url=>{this.pic = url+""});
    });
  }

}
