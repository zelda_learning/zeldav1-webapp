import { Component, OnInit, Input } from '@angular/core';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import{ flatMap, map } from 'rxjs/operators';


@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
  @Input() id: String;
  @Input() questionNum : number;
  image : String;
  options : any;
  optionNums : string[]
  keys : number [];
  letters = "abcdefghijklmnopqrstuvwxyz";
  @Input() realAns: string;
  correctOption : string;
  correctOptionPic : string;
  @Input() ans : string ;
  correct: string = "";

  constructor(private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.optionNums = [];
    this.options = {};
    this.correctOptionPic = "";
    this.database.getDownloadUrl("/Quizzes/"+this.id+"/"+this.questionNum + "/"+this.questionNum+".jpg").subscribe(
      data =>{
        this.image = data;
      }
    );

    this.keys = [0,1,2,3]

    for(let i in this.keys){
      var letter = this.letters[i];
      this.database.getDownloadUrl("/Quizzes/"+this.id+"/"
        +this.questionNum+"/"+this.questionNum+letter+".jpg").pipe(
          map(data=>{
            this.options[i] = (data);
            this.optionNums.push(i);
          })
        ).subscribe();
    }

    this.correctOption = this.realAns;
    this.database.getDownloadUrl("/Quizzes/"+this.id+"/"+this.questionNum+"/"+this.questionNum+this.correctOption+".jpg").subscribe(
      data=>{
        this.correctOptionPic = data;
      }
    )
    this.checkAnswer();
  }

  checkAnswer(){
    window.scrollTo(0,0);
    if(!this.ans || this.ans == ""){
      this.correct = "";
    }
    else{
      if(this.ans == this.realAns){
        this.correct = "correct";
      }
      else{
        this.correct = "incorrect"
      }
    }
  }

  isCorrect(){
    return this.ans == this.realAns;
  }

  getAnswer(){
    return  this.ans;
  }

  onChange(i:number){
    this.ans = this.letters[i];
  }

  isChecked(i:number){
    if(this.letters[i] == this.ans){
      return true;
    }
    else
      return false;
  }

}
