import { NgModule } from '@angular/core';
import {RouterModule , Routes} from '@angular/router';

import {CompanyComponent} from './company/company.component';
import {ApplicationComponent} from './application/application.component';
import {QuizComponent} from './quiz/quiz.component';
import {HomeComponent} from './home/home.component';
import {UniversityComponent} from './university/university.component';
import {ProfileComponent} from './profile/profile.component';
import {ArticleComponent} from './article/article.component';
import {ArticleViewComponent} from './article-view/article-view.component';
import {CompanyViewComponent} from './company-view/company-view.component';
import {UniversityViewComponent} from './university-view/university-view.component';
import {ApplicationViewComponent} from './application-view/application-view.component';
import {LoginComponent} from './login/login.component';
import {SignUpChatComponent} from './sign-up-chat/sign-up-chat.component';
import {SignupComponent} from './signup/signup.component';
import {QuizViewComponent} from './quiz-view/quiz-view.component';
import {CheckAnswersComponent} from './check-answers/check-answers.component';
import {TermsComponent} from './terms/terms.component';
import {FaqComponent} from './faq/faq.component';
import {IntroQuizComponent} from './intro-quiz/intro-quiz.component';
import {PersonalityComponent} from './personality/personality.component';
import {ApplicationFormComponent} from './application-form/application-form.component';
import {ApplicationSubmitComponent} from './application-submit/application-submit.component';
import {ApplicationSubmitIntroComponent} from './application-submit-intro/application-submit-intro.component';
import {MessagingComponent} from './messaging/messaging.component';

import {AuthguardService} from './authguard.service';
import {ThankyouComponent} from './thankyou/thankyou.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: "full"},
  { path: 'login', component: LoginComponent},
  { path: 'home', component: HomeComponent, canActivate :[AuthguardService]},
  { path: 'quizzes', component: QuizComponent,  canActivate :[AuthguardService],  data: { shouldReuse: true }},
//  { path: 'companies', component: CompanyComponent, canActivate :[AuthguardService]},
  { path: 'universities', component: UniversityComponent, canActivate :[AuthguardService],  data: { shouldReuse: true }},
  { path: 'profile', component: ProfileComponent, canActivate :[AuthguardService]},
  { path: 'profile/:id', component: ProfileComponent, canActivate :[AuthguardService]},
  { path: 'applications', component: ApplicationComponent, canActivate :[AuthguardService],  data: { shouldReuse: true }},
  { path: 'articles', component: ArticleComponent, canActivate :[AuthguardService],  data: { shouldReuse: true }},
  { path: 'articles/:id', component : ArticleViewComponent, canActivate :[AuthguardService],  data: { shouldReuse: true }},
  {path : 'quizzes/:id', component : QuizViewComponent, canActivate :[AuthguardService],  data: { shouldReuse: true }},
  //{ path : 'companies/:id', component: CompanyViewComponent, canActivate :[AuthguardService]},
  { path : 'universities/:id', component: UniversityViewComponent, canActivate :[AuthguardService],  data: { shouldReuse: true }},
  { path : 'applications/:id', component: ApplicationViewComponent, canActivate :[AuthguardService],  data: { shouldReuse: true }},
  { path : 'applications/:id/apply', component: ApplicationFormComponent},
  { path : 'applications/:id/intro', component: ApplicationSubmitIntroComponent},
  { path : 'applications/:id/apply/:comp', component: ApplicationFormComponent},
  { path : 'signup', component: SignUpChatComponent},
  { path : 'checkAnswers/:id', component:CheckAnswersComponent, canActivate :[AuthguardService]},
  { path : 'terms_and_conditions', component:TermsComponent},
  { path : 'FAQs', component:FaqComponent},
  { path : 'introquiz', component:IntroQuizComponent, canActivate : [AuthguardService]},
  { path : 'personality', component:PersonalityComponent, canActivate :[AuthguardService],  data: { shouldReuse: true } },
  { path : 'messages', component:MessagingComponent, canActivate :[AuthguardService],  data: { shouldReuse: true } },
  { path : 'thankyou', component: ThankyouComponent},
  {path : '**', redirectTo: '/home', pathMatch: "full"}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'}) ],
  exports : [
    RouterModule
  ],
  providers: [
    AuthguardService
  ]
})

export class AppRoutingModule {
}
