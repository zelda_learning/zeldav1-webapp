import { Component, OnInit } from '@angular/core';

import { FirebaseIntegrationService } from '../firebase-integration.service';
import { ProfileService} from '../profile.service';


@Component({
  selector: 'app-messaging',
  templateUrl: './messaging.component.html',
  styleUrls: ['./messaging.component.scss']
})
export class MessagingComponent implements OnInit {
  messages: any;
  messageText : string = '';

  constructor(private database: FirebaseIntegrationService, private profile: ProfileService) { }

  ngOnInit() {
    this.profile.messages = false;
    this.database.messagesRead();
    this.database.getUserMessages().subscribe(data=>{
      this.messages = data;
    });
  }

  sendMessage(){
    if( this.messageText != ''){
      var message = {
        messageSendTime :  new Date(),
        messageText : this.messageText,
        messageZeldaTeam : false
      }
      this.messageText = "";
      this.database.sendUserMessage(message);
    }
  }

}
