import { Component, OnInit } from '@angular/core';
import { ResourceObject} from '../objects/resourceObject';
import { FirebaseIntegrationService} from '../firebase-integration.service';


@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {
  allRes : ResourceObject[];
  pic : String [];

  constructor(private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.pic = [];

    this.database.getResources().subscribe(data =>{
      this.allRes = data;
      for(let i of data){
        this.database.getDownloadUrl("/Resources/"+ i.id+".jpg").subscribe(url =>{
          this.pic.push(url);
        });
      }
    });
  }

}
