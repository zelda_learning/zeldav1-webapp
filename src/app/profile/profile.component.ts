import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from '../profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit, OnDestroy {
  url : string;

  constructor( private route: ActivatedRoute,
      private router : Router, public  profile : ProfileService) { }

  ngOnInit() {
    window.scrollTo(0,0);
      this.route.params.subscribe(url=>{
        if(url.id){
          this.url = url.id;
        }
        else{
          this.url = "";
        }
      });
  }

  ngOnDestroy() {
    this.profile.update();
  }

  update(){
    this.profile.update();
  }


    getClass(x):string{
      if(x == this.url){
        return "nav-link active";
      }
      else{
        return "nav-link";
      }

    }



}
