import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import {ApplicationService} from '../application.service';
import {ProfileService} from '../profile.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  password: string;
  email: string;
  error : string;
  signedIn = false;
  id = "-GRRRRRRL"

  constructor(public profile : ProfileService, private database: FirebaseIntegrationService, private router: Router, private appService : ApplicationService) { }

  ngOnInit() {
  }

  logIn(){
    this.database.signUp(this.email,this.password).then(su=> {
      this.database.logInNoMenu(this.email,this.password).then((data)=>{
        console.log(this.profile.uid);
          this.database.createUserState(this.profile.uid).then((data)=>{
          this.database.createUserPersonal(this.profile.uid).then((data)=>{
          this.database.createUserSchool(this.profile.uid).then((data)=>{
            this.database.loggedIn = true;
            this.signedIn = true;
            this.appService.initUnauth();
            this.error = "";
            //this.router.navigate(["applications/-GRRRRRRL/apply/0"]);
          });
          });
          });
        });
      })
      .catch((error)=>{
        this.error = "A user already exists with this email address or the email address is incorrect";
      });
  }


}
