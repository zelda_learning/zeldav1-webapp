import { Component, OnInit } from '@angular/core';
import { Article } from '../objects/article';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  articles: Article[];

  constructor(private database : FirebaseIntegrationService, private router: Router) { }

  ngOnInit() {
      this.database.getResources().subscribe(data =>{
        this.articles = data;
      });
  }

}
