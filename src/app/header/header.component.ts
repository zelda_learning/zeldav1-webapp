import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { ProfileService } from '../profile.service';
import { FirebaseIntegrationService } from '../firebase-integration.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit{
  dropClass = "btn-group dropleft";
  constructor(private router : Router, public profile : ProfileService, public database : FirebaseIntegrationService) { }

  ngOnInit() {
    console.log(screen.width);
    this.database.getNewMessages().subscribe((data: any) => {
      if (data && data.newMessageReceived) {
        this.profile.messages = data.newMessageReceived;
      }
    });
    if(screen.width<=380){
        this.dropClass = "btn-group dropright";
    }
    if(this.profile.menu){
      this.profile.hClass = "col-xl-10 ml-auto fixed-top py-0 top-navbar";
    }
    else{
      this.profile.hClass = "col-xl-12 ml-auto fixed-top py-0 top-navbar";
    }
  }

  clickToggle(){
      this.profile.menClass = "col-xl-2 col-lg-3 col-md-4 sidebar fixed-top active";
      this.profile.menu =true;
      this.profile.hClass = "col-xl-10 ml-auto fixed-top py-0 top-navbar";
      this.database.rClass ="col-md-10 ml-auto margin" ;
  }

  logOut(){
    this.database.logOut();
    this.database.rClass = "ml-auto";
    this.router.navigate(["/login"]);
  }

}
