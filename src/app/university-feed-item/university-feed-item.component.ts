import { Component, OnInit, Input } from '@angular/core';
import { FirebaseIntegrationService} from '../firebase-integration.service';

@Component({
  selector: 'app-university-feed-item',
  templateUrl: './university-feed-item.component.html',
  styleUrls: ['./university-feed-item.component.scss']
})
export class UniversityFeedItemComponent implements OnInit {
  @Input() item : any;
  pic : String;
  cont : String;

  constructor(private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.pic ="";
    this.cont = this.item.closingDate.replace(/<\/?[^>]+(>|$)/g, "");
    this.database.getDownloadUrl("/Universities/" + this.item.id + ".jpg").subscribe(url =>{
      this.pic = url;
    });
  }

}
