import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UniversityFeedItemComponent } from './university-feed-item.component';

describe('UniversityFeedItemComponent', () => {
  let component: UniversityFeedItemComponent;
  let fixture: ComponentFixture<UniversityFeedItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UniversityFeedItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UniversityFeedItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
