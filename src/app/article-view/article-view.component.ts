import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-article-view',
  templateUrl: './article-view.component.html',
  styleUrls: ['./article-view.component.scss']
})
export class ArticleViewComponent implements OnInit {
  article : any;
  id : string;
  pic : string;

  constructor(private route : ActivatedRoute,
      private database : FirebaseIntegrationService,
      private router: Router) {}

  ngOnInit() {
    window.scrollTo(0,0);
      this.article = {};
      this.pic = "";
      this.route.params.subscribe(inp =>{
        this.id = inp.id;
        this.database.getResource(this.id).subscribe(data =>{
          this.article = data;
        });
        this.database.getDownloadUrl("/Resources/" + this.id +".jpg").subscribe(data=>{
          this.pic = data;
        });
      });
  }

}
