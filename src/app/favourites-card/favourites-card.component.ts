import { Component, OnInit } from '@angular/core';
import {FavouriteService } from '../favourite.service';
import {switchMap, map} from 'rxjs/operators';

@Component({
  selector: 'app-favourites-card',
  templateUrl: './favourites-card.component.html',
  styleUrls: ['./favourites-card.component.scss']
})
export class FavouritesCardComponent implements OnInit {
  favourites : Array<any>;
  numFav :number = 0;

  constructor(private favService : FavouriteService) { }

  ngOnInit() {
    this.favService.getFavourites().then(data=>{
      data.pipe(
        map(fav=>{
          return fav;
        })
      ).subscribe(data=>{
        this.favourites = data.pinnedBursaries;
        this.numFav = this.favourites.length;
      });
    });
  }

}
