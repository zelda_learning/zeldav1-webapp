import { TestBed, inject } from '@angular/core/testing';

import { FirebaseIntegrationService } from './firebase-integration.service';

describe('FirebaseIntegrationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FirebaseIntegrationService]
    });
  });

  it('should be created', inject([FirebaseIntegrationService], (service: FirebaseIntegrationService) => {
    expect(service).toBeTruthy();
  }));
});
