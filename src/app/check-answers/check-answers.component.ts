import { Component, OnInit, ViewChildren, QueryList } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import { QuestionComponent}from '../question/question.component';
import { Router } from '@angular/router';
import { QuizAnswerService} from '../quiz-answer.service';
import { map, catchError, switchMap} from "rxjs/operators";
import {of} from "rxjs";

@Component({
  selector: 'app-check-answers',
  templateUrl: './check-answers.component.html',
  styleUrls: ['./check-answers.component.scss']
})
export class CheckAnswersComponent implements OnInit {
  length : number
  quiz : any;
  id : string;
  pic : string;
  questions : any[];
  answers : any;
  keys : any[];
  @ViewChildren(QuestionComponent) questionComps :  QueryList<QuestionComponent>;

  constructor(private route : ActivatedRoute,
      private database : FirebaseIntegrationService,
      private router: Router,
      private ansService : QuizAnswerService) { }

  ngOnInit() {
      this.quiz = {};
      this.answers = {};
      this.pic = "";
      this.questions = [];
      this.route.params.pipe(
        switchMap(url=>{
          this.id = url.id;
          if(this.id!=this.ansService.id){
            this.router.navigate(['/quizzes']);
            return of("");
          }
          this.answers = this.ansService.answers;
          return this.database.getQuiz(this.id);
        }),
        switchMap(quiz=>{
          console.log(quiz);
          this.quiz = quiz;
          this.length = this.quiz.answers.length;
          return this.database.getDownloadUrl("/Quizzes/" + this.id +"/" +this.id +".jpg").pipe(
            map(url=>{return url}),
            catchError(err=>{return this.database.getDownloadUrl("/Quizzes/" + this.id +"/"+this.id+".png")}),
            map(url=>{return url})
          )
        })
      ).subscribe(url=>{
        this.pic = url;
      });
  }

  checkAnswers(){
    this.questionComps.forEach(
      data=>{
        data.checkAnswer();
      }
    );
  }

  getAnswers(){
    this.questionComps.forEach(
      data=>{
        this.answers[data.questionNum] = data.getAnswer();
      }
    );
    this.ansService.answers = this.answers;
    this.ansService.id = this.quiz.id;
    this.router.navigate(['/checkAnswers/'+this.quiz.id])
  }

  getChecked(i: number){
    var ans = this.answers[i];
    if(ans){
      return ans;
    }
    else{
      return "";
    }
  }

}
