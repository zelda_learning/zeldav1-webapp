import { Component, OnInit } from '@angular/core';
import { FirebaseIntegrationService} from '../firebase-integration.service';

@Component({
  selector: 'app-tips',
  templateUrl: './tips.component.html',
  styleUrls: ['./tips.component.scss']
})
export class TipsComponent implements OnInit {
  allRes : any[];
  userProgress : number;


  constructor(private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.database.getResources().subscribe(data =>{
      this.allRes = data;
    });
  }

}
