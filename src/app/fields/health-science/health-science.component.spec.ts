import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HealthScienceComponent } from './health-science.component';

describe('HealthScienceComponent', () => {
  let component: HealthScienceComponent;
  let fixture: ComponentFixture<HealthScienceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HealthScienceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HealthScienceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
