import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EbeComponent } from './ebe.component';

describe('EbeComponent', () => {
  let component: EbeComponent;
  let fixture: ComponentFixture<EbeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EbeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EbeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
