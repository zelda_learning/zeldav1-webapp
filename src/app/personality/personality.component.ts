import { Component, OnInit } from '@angular/core';
import { FirebaseIntegrationService} from '../firebase-integration.service';
import{ map} from 'rxjs/operators';

@Component({
  selector: 'app-personality',
  templateUrl: './personality.component.html',
  styleUrls: ['./personality.component.scss']
})
export class PersonalityComponent implements OnInit {
  data :any ;
  options :any;
  ready = false;
  field : string ="";

  constructor(private database : FirebaseIntegrationService) {}

  ngOnInit() {
    this.data = {
      labels: [],
      datasets: [
        {
          label: "Fields you might like",
          data: [],
          backgroundColor : [ '#803690', '#d9508a','#fff78c', '#c0ff8c', '#ffd08c', '#8ceaff']
        }
      ]
    };
    this.options = {
      cutoutPercentage :50,
      responsive: true,
      maintainAspectRatio: true,
      colors : [ '#ff8c9d', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360']
    };

    this.database.getUserPersonality().then(data=>{
      data.pipe(
        map(fields=>{
          return fields;
        })
      )
      .subscribe(data =>{
        if(data){
          this.data["labels"] = Object.keys(data["fieldML"]);
          for(let i of this.data["labels"]){
            this.data.datasets[0].data.push(data["fieldML"][i]);
          }
          this.ready=true;
        }
      });
    });
  }

  clickElement(e : any){
    this.field = this.data.labels[e[0]._index];
    console.log(this.field + " " + e[0]._index);
  }

}
