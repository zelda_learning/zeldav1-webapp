import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationSubmitFragmentComponent } from './application-submit-fragment.component';

describe('ApplicationSubmitFragmentComponent', () => {
  let component: ApplicationSubmitFragmentComponent;
  let fixture: ComponentFixture<ApplicationSubmitFragmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationSubmitFragmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationSubmitFragmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
