import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute} from '@angular/router';
import { FirebaseIntegrationService} from "../firebase-integration.service";
import { Router} from '@angular/router';
import { map, switchMap} from "rxjs/operators";
import {combineLatest} from "rxjs";
import {ApplicationService} from "../application.service";


@Component({
  selector: 'app-application-submit-fragment',
  templateUrl: './application-submit-fragment.component.html',
  styleUrls: ['./application-submit-fragment.component.scss']
})
export class ApplicationSubmitFragmentComponent implements OnInit {
  @Input() section : number;
  numSections : number;
  id : string;
  @Input() userFormId: string;
  formValuesNames : string[];
  formAnswers : any = {};
  formValues : any [];
  formOptions : any;
  letters = "ABCDEFGH";

  constructor(private database: FirebaseIntegrationService, private router: ActivatedRoute, private router2 : Router, private appService : ApplicationService) { }

  ngOnInit() {
    console.log("Entering frag");
    this.router.params.subscribe(url=>{
      this.id = url.id;
      if(!this.appService.form[this.letters[this.section]]){
        this.router2.navigate(["/applications/"+this.id+"/intro"])
      }
      this.database.getApplicationForm(this.id , this.letters[this.section]).subscribe(form=>{
        this.formValues = form["formValues"];
        this.formAnswers = this.appService.form[this.letters[this.section]].formValues;
        this.formValuesNames = Object.keys(this.formValues);
      });
    });
  }

  next(){
  }

  save(){
    console.log("updating");
    var newFormValues = {formValues:{}};
    for(let i of this.formValuesNames){
      newFormValues.formValues[i] = this.formAnswers[i];
    }
    this.database.updateApplicationForm(this.letters[this.section],this.userFormId,newFormValues);
  }

  submit(){
    this.save();
    //this.database.submitUserApplicationForm(this.userFormId);
    //this.router2.navigate(["/home"]);
  }
}
