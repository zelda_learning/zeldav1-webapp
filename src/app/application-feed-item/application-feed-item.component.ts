import { Component, OnInit, Input } from '@angular/core';
import { FirebaseIntegrationService} from '../firebase-integration.service';

@Component({
  selector: 'app-application-feed-item',
  templateUrl: './application-feed-item.component.html',
  styleUrls: ['./application-feed-item.component.scss']
})
export class ApplicationFeedItemComponent implements OnInit {
  @Input() item : any;
  pic : String;
  cont : String;

  constructor(private database : FirebaseIntegrationService) { }

  ngOnInit() {
    this.cont = this.item.subtitle.replace(/<\/?[^>]+(>|$)/g, "");
    this.pic ="";
    this.database.getDownloadUrl("/Bursaries/" + this.item.id + ".jpg").subscribe(url =>{
      this.pic = url;
    });
  }

}
