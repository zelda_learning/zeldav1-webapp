import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplicationFeedItemComponent } from './application-feed-item.component';

describe('ApplicationFeedItemComponent', () => {
  let component: ApplicationFeedItemComponent;
  let fixture: ComponentFixture<ApplicationFeedItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationFeedItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationFeedItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
