import { Component, OnInit } from '@angular/core';
import {FirebaseIntegrationService} from '../firebase-integration.service';

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.scss']
})
export class ThankyouComponent implements OnInit {

  constructor(private database: FirebaseIntegrationService) { }

  ngOnInit() {
    this.database.logOut();
  }

}
