import { Injectable } from '@angular/core';
import { FirebaseIntegrationService } from './/firebase-integration.service';

@Injectable()
export class FavouriteService {
  userState : any = {pinnedBursaries:[]};

  constructor(private database : FirebaseIntegrationService) {
    this.database.getUserState().then(data=>{
      data.subscribe(data=>{
        if(data){
            this.userState = data;
        }
      });
    });
  }

  getFavourites(){
    return this.database.getUserState();
  }

  isFavourite(bursaryId : string){
    if(this.userState.pinnedBursaries.indexOf(bursaryId)>-1)
      return true;
    else
      return false;
  }

  add(bursaryId : string){
    this.userState.pinnedBursaries.push(bursaryId);
    this.database.updateFav(this.userState.pinnedBursaries);
  }

  remove(bursaryId : string){
    this.userState.pinnedBursaries.splice(this.userState.pinnedBursaries.indexOf(bursaryId),1);
    this.database.updateFav(this.userState.pinnedBursaries);
  }
}
