import { Component, OnInit } from '@angular/core';
import {MenuItem} from '../objects/menuItem';
import {MENUDATA} from '../objects/menu.data';
import {FirebaseIntegrationService} from '../firebase-integration.service';
import {ProfileService} from '../profile.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  menuItems : MenuItem[];
  pic : string = "";
  up : any;
  menClass ="";
  constructor(public database : FirebaseIntegrationService, public profile : ProfileService) { }

  ngOnInit() {
    this.menClass = this.profile.menClass;
    this.menuItems = MENUDATA;
  }

  clickToggle(){
    this.profile.menClass = "col-xl-2 col-lg-3 col-md-4 sidebar fixed-top notactive";
    this.profile.menu = false;
    this.profile.hClass = "col-xl-12 ml-auto fixed-top py-0 top-navbar";
    this.database.rClass = "row-md-12 mx-3 margin";
  }

  toggleMaybe(){
    if(screen.width<1200){
      this.profile.menClass = "col-xl-2 col-lg-3 col-md-4 sidebar fixed-top notactive";
      this.profile.menu = false;
      this.profile.hClass = "col-xl-12 ml-auto fixed-top py-0 top-navbar";
    }
  }

}
